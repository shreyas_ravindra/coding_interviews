 //@Author -- Shreyas
//@Company -- Philips Electronics Nederland 
//@Brief -- Collection of common C programs which are asked in interviews 

//Programs collection --> 
//program to find a duplicate in an integer array taking input from the user
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main(int argc, const char * argv[])
{
    //program to count duplicates in an array
    int array [100];
    int i = 0x0;
    int number = 0x0;
    int count = 0x0;
    //int flag = 0x0;
    
    for(i =0 ; i < 100 ; i++)
        array[i] = i*2;
    
    i = 0x0;
    // make some duplicate values
    while (i < 100)
    {
        i = i+ 10;
        array[i] =100;
    }
    printf("enter the number whose duplicates you want to find = ");
    scanf("%d", &number);
    
    for(i=0 ; i < 100 ; i++)
    {
        if(array[i] == number)
        {
            count++;
        }
        //else continue the loop
    }
    printf("total duplicates is = %d ", count);
    
    return 0;
}

// program to see if there are duplicates in an array!!
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>

int main(int argc, const char * argv[])
{
    //program to count duplicates in an array
    int array [100];
    int i = 0x0, j=0x0;
    //int number = 0x0;
    //int count = 0x0;
    _Bool  flag = false;
    int temp;
    
    for(i =0 ; i < 100 ; i++)
        array[i] = i*2;
    
    i = 0x0;
    // make some duplicate values
    while (i < 100)
    {
        i = i+ 10;
        array[i] =100;
    }
    for( i = 0; i < 100 ; i++)
    {
        temp = array[i];
        for (j = 0; j < 50 ; j++)
        {
            if((array[j] == temp) && (j!=i))
            {
                //duplicate detected in 1st half only
                flag = true;
                break;
            }
        }
        for(j = 99; j>= 0; j--)
        {
            if(array[j] == temp && (j!=i))
            {
                //duplicate detected in 2nd half
                flag = true;
                break;
            }
                
        }
        flag = false;
    }
    if (flag == true)
    {
        printf("There are no duplicates in the array");
        return 0x0;
    }
    else
    {
        //if flag is TRUE
        printf("The array has duplicate data");
        return 0x0;
    }

}

// program to make the intersection of two arrays
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    UINT8 array1[10]={0x0};
    UINT8 array2[10]={0x0};
    UINT8 intersection[10]={0};
    UINT8 temp = 0x0;
    int i =0x0 , j= 0x0, k= 0x0;
    
    for(i=0; i< 10; i++)
        array1[i] = i*10;
    
    for(i=0; i<10; i++)
        array2[i] = i*5;
    
    //parse the 2nd array now
    for(i=0; i < 10; i++)
    {
        temp = array2[i];
        for(j=0; j< 10; j++)
        {
            if(array1[j] == temp)
            {
                intersection[k] = temp;
                k++;
                if(k > sizeof(intersection))
                {
                    printf("Something bad happenend here \n");
                    return FAIL;
                }
                break;
            }
        }
    }
    
    //print the intersected array
    printf("Printing the Arrays \n");
    for(i =0; i < sizeof(intersection); i++)
    {
        if(i == k)
        {
            break;
        }
        printf("%d \n", intersection[i]);
    }
    
    return SUCCESS;
}

// program to find the factorial of a number
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    int number=0x0, fact= 0x1;
    printf("Enter a number whose factorial needs to be found = ");
    scanf("%d",&number);
    
    for(int i =number ; i >= 1; i--)
    {
        fact= fact* i;
    }
    printf("\n The factorial of number %d is %d",number,fact);
    
    return SUCCESS;
}

//prorgam to find whether a number is a power of 2 or not
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    int number = 0x0;
    int temp = 0x0;
    printf("Enter a number = ");
    scanf("%d",&number);
    temp = number & (number - 1);
    
    if(temp == 0x0)
    {
        printf("The number is a Power of 2 \n ");
    }
    else
    {
        printf("The number is not a Power of 2 \n");
    }
    
    return SUCCESS;
}

//prorgam to find whether a given number is an armstrong number
//an armstrong number is a number whose sum of cubes of individual
//digits is equal to the number it self
//for example 3^3 + 7^3 + 1^3 = 371
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    int number = 0x0;
    int sum =0;
    int temp =0x0;
    int digit =0x0;
    printf("Enter a number =");
    scanf("%d", &number);
    
    //store the number in temp
    temp = number;
    while(number!= 0)
    {
        digit = number % 10;
        number = number/10;
        sum = sum + (digit* digit * digit);
        digit = 0x0;
    }
    if (sum == temp)
    {
        printf("The given number is an Armstrong number \n");
    }
    else
    {
        printf("The given number is NOT an Armstrong number \n ");
    }
    return SUCCESS;

}

//
//  main.c
//  Hello World
//
//  Created by Shreyas Ravindra on 21/06/14.
//
//
//program to check whether a number is a palindrome
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    int number = 0x0;
    int temp =0x0;
    int reverse =0x0;
    printf("Enter a number =");
    scanf("%d", &number);
    
    //store the number in temp
    temp = number;
    while(number!= 0)
    {
        reverse = reverse*10;
        reverse = reverse + number%10;
        number = number/10;
    }
    if (reverse == temp)
    {
        printf("The given number %d is a PALINDROME \n", temp);
    }
    else
    {
        printf("The given number %d is NOT a PALINDROME \n ",temp);
    }
    return SUCCESS;

}

//program to print N numbers in Fibonacci series
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    int first = 0x0;
    int second = 0x1;
    int third = 0x0; // third number is always the sum of first and second
    int count = 0x0;
    
    printf ("\n Enter how many numbers in the series you want?? ");
    scanf("%d",&count);
    
    if (count < 2)
    {
        printf("INVALID COUNT");
    }
    else
    {
        printf("\n Printing the Series");
        printf(" %d , %d ",first,second);
        count = count -2;
        while(count > 0)
        {
            third = first + second;
            printf(" %d ",third);
            first = second;
            second = third;
            count = count -1;
        }
    }
    return SUCCESS;

}

//program to reverse the contents of the array
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    UINT8 array[10] = {1,2,3,4,5,6,7,8,9,10};
    UINT8 i = 0x0;
    UINT8 j = sizeof(array);
    
    printf("\n Original array is = ");
    for(i =0 ; i<10 ; i++)
    {
        printf("%d \n", array[i]);
    }
    
    j= (sizeof(array)-1);
    
    for(i = 0; i < sizeof(array)/2; i++)
    {
        array[i] = array[i]^array[j];
        array[j] = array[i]^array[j];
        array[i] = array[i]^array[j];
        j--; //i would ensure that the overflow doesn't happen
    }
    
    
    printf("\n Reversed array is = ");
    for(i =0 ; i<10 ; i++)
    {
        printf("%d \n", array[i]);
    }
    return SUCCESS;
}

//program to insert an element into the array
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    int array[10] = {1,2,3,4,5,0,0,0,0,0};
    int i = 0x0;
    int position = 0x0;
    int last_valid_position = 0x0;
    int number_to_enter = 0x0;
    
    printf("\n Original array is = ");
    for(i =0 ; i<10 ; i++)
    {
        printf("%d \n", array[i]);
    }
    
    printf("\n Enter the position where you want to insert = ");
    scanf("%d",&position);
    
    printf("\n Enter the number you want to intert = ");
    scanf("%d", &number_to_enter);
    
    //get the position of the last valid entry
    for(i =0 ; i < 10; i++)
    {
        if(array[i] == 0x0)
        {
            last_valid_position = i;
            break;
        }
    }
    // moove the other elements down
    for(i= (last_valid_position +1); i > position; i--)
    {
        array[i] = array[i-1];
    }
    //store the number
    array[position] = number_to_enter;
    
    printf("\n The new array is =");
    for(i =0 ; i<10 ; i++)
    {
        printf("%d \n", array[i]);
    }
    
    return SUCCESS;
}


//program to delete an element into the array
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    int array[10] = {1,2,3,4,5,6,0,0,0,0};
    int i = 0x0;
    int position = 0x0;
    int last_valid_position = 0x0;
    
    printf("\n Original array is = ");
    for(i =0 ; i<10 ; i++)
    {
        printf("%d \n", array[i]);
    }
    
    printf("\n Enter the position where you want to delete = ");
    scanf("%d",&position);
    
    
    //get the position of the last valid entry
    for(i =0 ; i < 10; i++)
    {
        if(array[i] == 0x0)
        {
            last_valid_position = i;
            break;
        }
    }
    // moove the other elements down
    for(i= position; i < last_valid_position; i++)
    {
        array[i] = array[i+1];
    }
    
    printf("\n The new array is =");
    for(i =0 ; i<10 ; i++)
    {
        printf("%d \n", array[i]);
    }
    
    return SUCCESS;
}

//program set the nth bit
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    UINT8 number = 0x35;
    int setter = 0x1;
    int bit_number = 0x0;
    printf("\n Enter the bit to be set (0 to 7) =");
    scanf("%d", &bit_number);
    
    setter = setter << bit_number;
    
    number = number|setter;
    
    printf("The number = 0x%x",number);
    
    return SUCCESS;
}

//program to clear the nth bit
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    UINT8 number = 0xF7;
    UINT8 mask = 0x1;
    int bit_number = 0x0;
    printf("\n Enter the bit to be cleared (0 to 7) =");
    scanf("%d", &bit_number);
    
    //prepare the correct mask
    mask = mask << bit_number;
    mask = mask^0xFF;
    
    number = number & mask;
    
    printf("The number = 0x%x",number);
    
    return SUCCESS;
}
//program to toggle the nth bit
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    UINT8 number = 0xFF;
    UINT8 mask = 0x1;
    int bit_number = 0x0;
    printf("\n Enter the bit to be toggled (0 to 7) =");
    scanf("%d", &bit_number);
    
    //prepare the correct mask
    mask = mask << bit_number;
    
    number = number ^ mask;
    
    printf("The number = 0x%x",number);
    
    return SUCCESS;
}
//write a program to find the number of 0's and 1's in a 32 bit number
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    int number = 0x0, variable = 0x0;
    int count_zeros = 0x0;
    int count_ones = 0x0;
    int i = 0x0;
    
    printf("Enter the 32 bit number =");
    scanf("%x", &number);
    for(i = 0x0; i < 32; i++)
    {
        variable = number & 0x1;
        if (variable == 0x1)
        {
            count_ones++;
        }
        else
        {
            count_zeros++;
        }
        number = number >> 0x1;
    }
    printf("\n The number of zeros is = %d", count_zeros);
    printf("\n The number of ones is = %d", count_ones);
    return SUCCESS;
}

//write a program to swap the even bits and the odd bits in a number
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

typedef unsigned char UINT8; 
typedef unsigned short int UINT16; 

//create a print binary function
void binaryPrint(UINT16 variable)
{
    int i = 0; 
    int temp = 0; 
    int mask = 0x8000; 
    printf("0b ");
    for(i=0; i<16; i++)
    {
        temp = variable & mask; 
        temp = temp >>15; 
        variable = variable << 0x1; 
        printf("%d",temp); 
    }
}
/*Write a C program to split the even and odd bits */
int main(void)
{
    UINT16 variable = 0xDEAD; 
    UINT16 temp1 = 0x0; 
    UINT16 temp2 = 0x0;
    printf("The variable is "); 
    binaryPrint(variable);
    temp1 = (variable >> 1)& 0x5555;
    temp2 = (variable << 1)& 0xAAAA; 
    variable = temp1 | temp2;
    printf("\n"); 
    printf("The new var is  "); 
    binaryPrint(variable);  
    return 0x0; 
}
//write a function to determine bitwise if a given number is a palindrome
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

void print_binary(int data);

int main(int argc, const char * argv[])
{
    int number = 0x0,var =0x0;
    int reversed_number = 0x0;
    int temp = 0x0; // variable to store a copy of the number
    int i = 0x0;
    printf("\nEnter the number = ");
    scanf("%x", &number);
    
    
    
    //print the number in binary
    printf(" \n the original number is =0b ");
    print_binary(number);
    printf(" \n the original number is =0x%x",number);
    //store the original number here
    temp = number;
    //enter the reversal logic here
    for(i = 31 ; i >= 0 ; i--)
    {
        var = number & 0x1;
        var = (var << i);
        reversed_number |= var; //start setting the reversed bits
        number = number >> 0x1 ;
        var = 0x0;
    }
    //get back the number
    number = temp;
    
    printf("\n the reversed number is =0b ");
    print_binary(reversed_number);
    printf(" \n the reversed number is =0x%x",reversed_number);
    
    if (number == reversed_number)
    {
        printf("\n The numbers IS A PALINDROME");
    }
    else
    {
        printf("\n The number is NOT a palindrome");
    }
    return SUCCESS;
}

void print_binary(int data)
{
    int i = 0x0;
    for(i =0; i < 32 ; i++)
    {
        printf("%x",(data & 0x1));
        data = data >> 0x1;
    }
    printf("\n");
}

// Single Linked Lists Problems1
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char
#define MAX_DEFAULT_LIST 5

struct node
{
    char data; //data element in a linked list
    struct node* nxt; //the next pointer
};

//API to create a node
static struct node* create_node()
{
    static int data = 0x1;
    struct node * data_node = (struct node*)malloc(sizeof(struct node));
    if(NULL!= data_node)
    {
        data_node->data = data;
        data_node->nxt = NULL;
        data++;
    }
    else
    {
        printf("\n Unable to create a NODE");
        return NULL;
    }
    return(data_node);
}
//API to get the length of the linked list
static int get_length_of_the_list(struct node* head_pointer)
{
    int count = 0x0;
    struct node* ptr = NULL;
    ptr = head_pointer;
    while(ptr!= NULL)
    {
        count++;
        ptr = ptr->nxt;
    }
    return count;
}

static struct node* create_linked_list(int no_of_elements)
{
    int i = 0x0;
    struct node* head = NULL;
    struct node* ptr= NULL;
    //now create a default list of elements here
    head = create_node();
    ptr= head;
    for(i = 0; i < (no_of_elements-1); i++)
    {
        ptr->nxt = create_node();
        if(ptr->nxt != NULL)
        {
            ptr= ptr->nxt;
        }
    }
    //terminate the linked list
    ptr->nxt = NULL;
    return head;
}

static void print_linked_list(struct node* head_pointer)
{
    struct node* ptr = NULL;
    ptr = head_pointer;
    while(ptr != NULL)
    {
        printf("%d", ptr->data);
        ptr = ptr->nxt;
        if(ptr!=NULL)
        {
            printf("-->");
        }
    }
    return;
}

static void add_new_node_in_end(struct node* head_pointer, int data)
{
    struct node* ptr = NULL;
    struct node* new = NULL;
    
    new=create_node();
    if(NULL != new)
    {
        //new node created successfully
        new->data = data;
        ptr = head_pointer;
        //traverse it till it sees a NULL
        while (ptr->nxt != NULL)
        {
            ptr = ptr->nxt;
        }
        ptr->nxt = new;
        new->nxt = NULL; // terminate
        printf(" \n Node added Successfully");
    }
    else
    {
        printf("\n Unable to add the element in the end");
    }
}

static void add_a_node_in_x_position(struct node* head_pointer, int position, int data)
{
    struct node* ptr = NULL;
    struct node* ptr_minus_one = NULL;
    struct node* new = NULL;
    int length = 0x0;
    int i = 0x1; // use actual indexes here and not array index
    
    length = get_length_of_the_list(head_pointer);
    //create the new node and keep
    new = create_node();
    new->nxt = NULL;
    new->data = data;
    // if position is greater than length of the linked list then don't add and return
    if(position > length)
    {
        printf("Position is INVALID");
        return;
    }
    else
    {
        //initialize both to head
        ptr = head_pointer;
        ptr_minus_one = head_pointer;
        while( i != position)
        {
            ptr_minus_one = ptr;
            ptr = ptr->nxt;
            i++;
        }
        ptr_minus_one->nxt = new;
        new->nxt = ptr;
        printf("\n NODE added successfully @ position %d",position);
        
    }
}
static void delete_element(struct node* head_pointer, int data)
{
    struct node* ptr = NULL;
    struct node* ptr_minus_one = NULL;
    ptr = head_pointer;
    ptr_minus_one = head_pointer;
    while(ptr->nxt != NULL)
    {
        if(ptr->data == data)
        {
            break;
        }
        ptr_minus_one = ptr;
        ptr = ptr->nxt;
    }
    if(ptr->nxt == NULL)
    {
        ptr_minus_one->nxt = NULL;
    }
    else
    {
        ptr_minus_one->nxt = ptr->nxt;
        ptr->nxt = NULL;
    }
    printf("\n Element %d DELETED ",data);
    return;
}

static bool find_element(struct node* head_pointer, int data)
{
    struct node* ptr = NULL;
    ptr = head_pointer;
    while(ptr !=NULL)
    {
        if(ptr->data == data)
        {
            return true;
        }
        ptr = ptr->nxt;
    }
    return false;
}

static void reverse_linked_list(struct node* head_pointer)
{
    struct node* p0;
    struct node* p1;
    struct node* p2 = NULL;
    
    p0 = head_pointer;
    while(p0 != NULL)
    {
        p1 = p2;
        p2 = p0;
        p0 = p0->nxt;
        p2->nxt = p1;
    }
    head_pointer = p2;
    printf("\n Reversed Linked List is =");
    print_linked_list(head_pointer);
    return;
}

int main(int argc, const char * argv[])
{
    
    int option = 0x0;
    struct node* head = NULL;
    //give a list of options to sort out
    printf("\n *************** Here are the list of options *********************");
    printf("\n1. Create a single linked list");
    printf("\n2. Add a new node at the end");
    printf("\n3. Delete a node");
    printf("\n4. Reverse the list");
    printf("\n5. Count the number of elements in a linked list");
    printf("\n6. Search an element in a linked list");
    printf("\n7. Print a linked List");
    printf("\n8. Add a node in x position");
    printf("\n100. exit");


GETINPUT:
    printf("\nEnter the option = ");
    scanf("%d", &option);
    
    switch(option)
    {
        case 1:
        {
            head = create_linked_list(MAX_DEFAULT_LIST);
            if(head == NULL)
            {
                printf("\nthe linked list could not be created");
            }
            else
            {
                printf("\n Singly Linked list is created successfully");
            }
            break;
        }
        case 2:
        {
            int element = 0x0;
            printf("Enter the element to be added =");
            scanf("%d", &element);
            add_new_node_in_end(head, element);
            break;
        }
        case 3:
        {
            int element =0x0;
            printf("Enter the element to be deleted = ");
            scanf("%d",&element);
            delete_element(head,element);
            break;
        }
        case 4:
        {
            reverse_linked_list(head);
            break;
        }
        case 5:
        {
            int count = 0x0;
            count = get_length_of_the_list(head);
            printf("The length of the list is %d",count);
            break;
            
        }
        case 6:
        {
            bool found = false;
            int data = 0x0;
            printf("\n Print the number to be found in the linked list = ");
            scanf("%d",&data);
            found = find_element(head, data);
            if(true == found)
            {
                printf("\n Element is present in the linked list");
            }
            else
            {
                printf("\n Element is not present in the linked list ");
            }
            break;
        }
        case 7:
        {
            printf("\n Printing the linked list \n");
            print_linked_list(head);
            break;
            
        }
        case 8:
        {
            int element = 0x0;
            int position = 0x0;
            printf(" \n Enter the position");
            scanf("%d", &position);
            printf("\n Enter the data");
            scanf("%d",&element);
            //call the API to add a node
            add_a_node_in_x_position(head,position,element);
            break;
        }
        case 100:
        {
            goto END;
        }
        default:
            printf("The code is not supported as yet");
            
    }
   goto GETINPUT;
END:
    return SUCCESS;
}
// Write a program to replace m -> m+5 bit in a 32 bit number
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

static void print_binary(int data)
{
    int i = 0x0;
    for(i =0; i < 32 ; i++)
    {
        printf("%x",(data & 0x1));
        data = data >> 0x1;
    }
    printf("\n");
}


int main(int argc, const char * argv[])
{
    int x = 0x0;
    int val_to_replace = 0x0; //5 bit value to replace;
    int start_position = 0x0; // this is m
    int end_position = 0x0;
    int mask = 0xFFFFFFFF;
    int temp_mask = 0x1;
    int i = 0x0;
    
    printf("\n Enter the data to be changed = ");
    scanf("%x", &x);
    
    print_binary(x);
    
    printf("\n enter position from 0->(26)");
    scanf("%d",&start_position);
    
    if (start_position > 26)
    {
        printf("\n Error in position number");
    }
    else
    {
        end_position = start_position +5;
    }
    
    //prepare the mask
    temp_mask = 0x1 << start_position;
    for(i = start_position; i <= end_position; i++)
    {
        mask = mask^temp_mask;
        temp_mask = temp_mask << 0x1;
    }
    
    printf("\n Enter the 5 bit value to be replaced =");
    scanf("%d", &val_to_replace);
    
    val_to_replace = val_to_replace & 0x1F;
    
    x= ((x & mask)|(val_to_replace << start_position));
    
    printf("Final replaced Value =");
    print_binary(x);
    
    return SUCCESS;
    
}

//Write a program to find the middle element in a linked list 
static void find_middle_element(struct node* head_pointer)
{
    int count = 0x0;
    int i = 0x1;
    struct node* ptr = NULL;
    
    ptr = head_pointer;
    
    //get the length of linked list
    count = get_length_of_the_list(head_pointer);
    
    if(count & 0x1) //odd linked list
    {
        while(i != (count+1)/2)
        {
            ptr=ptr->nxt;
            i++;
        }
        printf(" \n middle element is = %d",ptr->data);
    }
    else //even linked list
    {
        while(i != count/2)
        {
            ptr = ptr->nxt;
            i++;
        }
        printf("\n Middle elements are = ");
        printf("%d -->",ptr->data);
        ptr = ptr->nxt;
        printf("%d",ptr->data);
    }
}

// implement the stringlen function and check
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

unsigned int my_strlen(char *char_pointer)
{
    unsigned int count = 0x0;
    while(*char_pointer != '\0')
    {
        count++;
        char_pointer= char_pointer+1;
    }
    return count;
}

int main(int argc, const char * argv[])
{
    char string[]= "Shreyas is a good boy";
    unsigned int count1 = 0x0;
    unsigned int count2 = 0x0;
    
    count1 = (unsigned int)strlen(string);
    
    printf("\n The size of string using lib is %u", count1);
    
    count2 = my_strlen(string);
    
    printf("\n The size of string is %u",count2);
    
    if(count1 == count2)
    {
        return SUCCESS;
    }
    return FAIL;
}

// implement the strcpy() function in C
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

unsigned int my_strlen(char *char_pointer)
{
    unsigned int count = 0x0;
    while(*char_pointer != '\0')
    {
        count++;
        char_pointer= char_pointer+1;
    }
    return count;
}

int main(int argc, const char * argv[])
{
    char destination[10];
    char source[10];
    int size_of_source = 0x0;
    int i = 0;
    
    printf("\n Enter the original string =");
    scanf("%s",source);
    size_of_source = my_strlen(source);
    
    if(sizeof(destination) < size_of_source)
    {
        printf("CANNOT COPY");
        return FAIL;
    }
    else
    {
        i =0x0;
        for(i =0; i < size_of_source; i++)
        {
            *(destination + i) = *(source + i);
        }
        destination[i] = '\0'; //terminate with NULL character
    }
    
    printf("\n DESTINATION = %s", destination);
    return SUCCESS;
    
}

// implement the strcat() function in C
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

unsigned int my_strlen(char *char_pointer)
{
    unsigned int count = 0x0;
    while(*char_pointer != '\0')
    {
        count++;
        char_pointer= char_pointer+1;
    }
    return count;
}

int main(int argc, const char * argv[])
{
    char destination[] = "This is";
    char source[] = "Shreyas Ravindra";
    int size_of_destination = 0x0;
    int i = 0x0;
    int size_source = 0x0;
    int j = 0;
    
    size_of_destination= my_strlen(destination);
    size_source = my_strlen(source);
    
    for(i=size_of_destination; i<(size_of_destination+size_source); i++ )
    {
        *(destination + i) = *(source + j);
        j++;
    }
    destination[i] = '\0';
    printf("String is --> %s\n",destination);
    return SUCCESS;
    
}

// implement the strcmp() function in C, returns 0x0 if equal
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

unsigned int my_strlen(char *char_pointer)
{
    unsigned int count = 0x0;
    while(*char_pointer != '\0')
    {
        count++;
        char_pointer= char_pointer+1;
    }
    return count;
}

int my_strcmp(char*s1, char*s2)
{
    int count1 = 0x0;
    int count2 = 0x0;
    char* ptr1 = NULL;
    char* ptr2 = NULL;
    
    count1 = my_strlen(s1);
    
    count2 = my_strlen(s2);
    
    ptr1 = s1;
    ptr2 = s2;
    
    if(count1 != count2)
    {
      while((*ptr1 ^ *ptr2) == 0x0)
      {
          ptr1++;
          ptr2++;
      }
      return ((*ptr1-*ptr2));

    }
    else //if count is equal
    {
        while(((*ptr1 ^ *ptr2) == 0x0) && *ptr1 != '\0')
        {
            ptr1++;
            ptr2++;

        }

    }
    return((*ptr1 - *ptr2));
}

int main(int argc, const char * argv[])
{
    char destination[] = "tpple";
    char source[] = "apple";
    int result = 0x0;
    result = my_strcmp(source,destination);
    if (0x0 == result)
    {
        printf("\n The strings are equal");
    }
    else
    {
        printf("The strings are not equal %d",result);
    }
    return SUCCESS;
}

// Program to reverse a string
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

unsigned int my_strlen(char *char_pointer)
{
    unsigned int count = 0x0;
    while(*char_pointer != '\0')
    {
        count++;
        char_pointer= char_pointer+1;
    }
    return count;
}

int main(int argc, const char * argv[])
{
    char original[] = "Shreyas is a good boy";
    char reverse[40] = {0};
    int i = 0x0;
    int j= 0x0;
    int count = 0x0;
    
    count = my_strlen(original);
    
    for(i=0,j=(count-1); (i <= count && j>=0); i++,j--)
    {
        reverse[i] = original[j];
    }
    reverse[i] = '\0';
    
    printf("\n Original String is ---> %s",original);
    
    printf("\nReversed String is --->  %s",reverse);
    
    return SUCCESS;
}

// Program to check if a string is a palidrome
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

unsigned int my_strlen(char *char_pointer)
{
    unsigned int count = 0x0;
    while(*char_pointer != '\0')
    {
        count++;
        char_pointer= char_pointer+1;
    }
    return count;
}

int main(int argc, const char * argv[])
{
    char string_buffer[100] = {0};
    int count = 0x0;
    int i =0x0;
    
    char* head = NULL;
    char* base = NULL;
    
    int flag = 0x1; // palindrome flag
    
    printf("\n Please enter the string to check = ");
    scanf("%s",string_buffer);
    //get the string length
    count = my_strlen(string_buffer);
    
    head = string_buffer;
    base = string_buffer + (count -1); // subtract 1 for array index!!
    if (count & 0x1) // this means length is odd
    {
        
        while (i < (count +1)/2)
        {
            if(*head != *base)
            {
                flag = 0x0;
                break;
            }
            head++;
            base--;
            i++;
        }
    }
    else //this means even length
    {
        while(i <= count/2)
        {
            if(*head != *base)
            {
                flag = 0x0;
                break;
            }
            i++;
        }
    }
    if(flag == 0x1)
    {
        printf("\n The given string is a PALINDROME");
    }
    else
    {
        printf("\n The given string is not a PALINDROME");
    }
    
    return SUCCESS;
}

// Program to linear sort the array of 10 elements
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int main(int argc, const char * argv[])
{
    int array[20] = {0};
    int i = 0x0;
    int j = 0x0;
    
    printf("\n Enter the array elements in any order ");
    for(i =0; i< 20; i++)
    {
        scanf("%d",(array+i));
    }
    
    printf("\n The original array elements are = ");
    for(i =0; i<20; i++)
    {
        printf("%d  ",*(array+i));
    }
    
    //sort the array
    for (j = 0; j< 19; ++j)
    {
        for(i = 0; i < 19-j ; ++i)
        {
            if(array[i] > array[i+1])
            {
                //swap the array elements without using a temp variables
                array[i] = array[i]^array[i+1];
                array[i+1] = array[i]^array[i+1];
                array[i] = array[i]^array[i+1];
            }
        }
        
    }
    //print the resultant sorted array
    printf("\n The array elements in ascending order is");
    for(i= 0; i<20; i++)
    {
        printf("%d  ", *(array+i));
    }
    
    return SUCCESS;
}


//Program to work with Function pointers
//Just a trial program to deal with function pointers 
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

typedef enum s {
    SUNDAY = 1,
    MONDAY = 2,
    TUESDAY = 3,
    WEDNESDAY = 4,
    THURSDAY = 5,
    FRIDAY = 6,
    SATURDAY = 7,
}s_days ;

s_days (*func)(int);

static s_days modify_day(int);

int main(int argc, const char * argv[])
{
    s_days today = SATURDAY;
    int new_day = 0x0;
    
    //assign the function pointer
    func = modify_day;
    printf ("Today is = ");
    
    
    switch(today)
    {
        case SUNDAY: printf("Sunday");
            break;
        case MONDAY: printf("Monday");
            break;
        case TUESDAY: printf("Tuesday");
            break;
        case WEDNESDAY: printf("Wednesday");
            break;
        case THURSDAY: printf("Thursday");
            break;
        case FRIDAY: printf("Friday");
            break;
        case SATURDAY: printf("Saturday");
            break;
        default:printf("Something is wrong");
            break;
    }
    
    printf("\n Change the day");
    printf("\n Enter any number between 1 to 7");
    scanf("%d",&new_day);
    
    printf("\n The New Day is ==");
    
    switch(func(new_day))
    {
        case SUNDAY: printf("Sunday");
            break;
        case MONDAY: printf("Monday");
            break;
        case TUESDAY: printf("Tuesday");
            break;
        case WEDNESDAY: printf("Wednesday");
            break;
        case THURSDAY: printf("Thursday");
            break;
        case FRIDAY: printf("Friday");
            break;
        case SATURDAY: printf("Saturday");
            break;
        default:printf("Something is wrong");
            break;
    }
    
    
    return SUCCESS;
}

static s_days modify_day(int day)
{
    //typecast and return
    return ((s_days)day);
}

//Working with Pointer to a Constant
// Address can change as long as you point to a constant
// But value cannot
// Program to linear sort the array of 10 elements
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

const int var = 100; // constant
const int hell = 666; // constant integer
const int cool = 555; // constant integer


int main(int argc, const char * argv[])
{
    int me = 2415;
    const int *p = &var;
    
    me++;
    ++me;
    --me;
    --me;
    ++me;
    printf("\n Value of P is Var = %d", *p);
    
    p = &hell;
    printf("\n Value of P is Hell = %d", *p);
    
    p = &cool;
    printf("\n Value of P is Cool = %d", *p);
    
    //note p can also be assigned to me which is upgraded to const.
    p = &me;
    printf("\n Value of P is me = %d", *p);
    //*p = 500; //not allowed now!
    
    return SUCCESS;
}

//Program to illustrate constant pointer to a type 
//Constant pointer to a type
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

const int var = 100; // constant
const int hell = 666; // constant integer
const int cool = 555; // constant integer
int new_var = 30;

int main(int argc, const char * argv[])
{
    int me = 2415;
    int *const p = &new_var;
    
    printf("\n Value of P is new_var = %d", *p);
    
    //p = &me;   //not allowed
    *p= (*p) + me;
    
    printf("\n Value of P is (*p) + me = %d", *p);
    
    return SUCCESS;
}

//Constant pointer to a Constant! 
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

const int var = 100; // constant
const int hell = 666; // constant integer
const int cool = 555; // constant integer
int new_var = 30;

int main(int argc, const char * argv[])
{
    const int *const p = &new_var;
    
    printf("\n Value of P is new_var = %d", *p);
    
    //p = &me;   //not allowed
    //*p= (*p) + me;
    
    return SUCCESS;
}

//Pointer to a constant character
//Here address can change but value pointed at 
//can't be changed 
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

char str[] = "Shreyas";
const char str1[] = "Ravindra";

int main(int argc, const char * argv[])
{
    const char *ptr = str;
    printf("\n Str is = %s", ptr);
    
    ptr = str1;
    printf("\n Str1 is = %s",ptr);
    
    ptr = "Hello World";
    printf("\n Surprise = %s",ptr);
    
    // const values cannot be changed
    //*(ptr+3) = 'f';
    
    return SUCCESS;
}

//constant pointer to a character 
//here address can't be changed but 
//value pointed to the address can be changed 
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

char str[] = "Shreyas";
const char str1[] = "Ravindra";

int main(int argc, const char * argv[])
{
    char *const ptr = str;
    printf("\n Str is = %s", ptr);
    
    //ptr = str1;
    //printf("\n Str1 is = %s",ptr);
    
    //ptr = "Hello World";
    //printf("\n Surprise = %s",ptr);
    
    //values can be changed
    *(ptr+3) = 'f';
    printf("\n Str changed is =%s", ptr);
    
    
    return SUCCESS;
}

//Constant character pointer to a constant character
//Here neither the address nor the value pointed by the 
//address can change. 
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

char str[] = "Shreyas";
const char str1[] = "Ravindra";

int main(int argc, const char * argv[])
{
    const char *const ptr = str;
    printf("\n Str is = %s", ptr);
    
    /** All the below things are invalid **/
    
    //ptr = str1;
    //printf("\n Str1 is = %s",ptr);
    
    //ptr = "Hello World";
    //printf("\n Surprise = %s",ptr);
    
    //values can be changed
    //*(ptr+3) = 'f';
    //printf("\n Str changed is =%s", ptr);
    
    
    return SUCCESS;
}

//The Address of register can't be taken
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char



int main(int argc, const char * argv[])
{
    register a, b, c;
    printf (" Enter 2 values = ");
    scanf("%d %d", &a, &b); //compilation error
    c = a + b;
    printf("Answer = %d",c);
    return SUCCESS;
}

// Program to generate random numbers in C from 1 to 100
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char



int main(int argc, const char * argv[])
{
    int i = 0x0;
    int random = 0x0;
    printf("The random numbers are as follows between 1 and 100");
    for ( i= 0; i < 10; i++)
    {
        random = rand() % 100;
        printf("\n %d",random);
    }
    return SUCCESS;
}

// Program to generate first N numbers of fibonacci series
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char



int main(int argc, const char * argv[])
{
    int i = 0;
    int j = 1;
    int sum = 0x0;
    int total_digits = 0x0;
    
    printf("\nEnter the number of digits you need ");
    scanf("%d", &total_digits);
    
    //first two digits of the series have to be printed as is
    //so just subtract 2 digits from the sequence
    total_digits -= 2;
    printf("\n The series are as follows \n");
    printf("%d , %d ,", i, j);
    
    while(total_digits != 0)
    {
        sum = i + j;
        printf("%d ,", sum);
        //reduce the count of digits as soon as you print the sum
        total_digits-= 1;
        i = j;
        j = sum;
    }
    return SUCCESS;
}

// Program to extract digits from a number
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

static void reverse( int *number);

int main(int argc, const char * argv[])
{
    int number = 0x0;
    int temp = 0x0;
    
    printf("\n Enter the number whose digits are needed ");
    scanf("%d",&number);
    
    temp = number;
    
    reverse(&number);
    
    if ((number == 0x1) && temp > 0x1 )
    {
        printf("Only digit is 1 and 0");
    }
    else if (number == 0x1 && temp == 0x1)
    {
        printf ("Only digit is 1");
    }
    else
    {
        printf("\n the digits in the number are as follows --> \n");
        while(number != 0)
        {
            printf("%d ,", number%10);
            number= number/10;
        }
    }
    
}

void reverse (int* number)
{
    int reverse = 0x0;
    int temp = 0x0;
    
    while ((*number) != 0)
    {
        reverse =reverse * 10;
        temp  = (*number)% 10;
        reverse = reverse + temp;
        *number = (*number)/10;
    }
    // now the number is reversed
    *number = reverse;
}

// Program delete consonants in a string
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

char name[] = "Shreyas";

int main(int argc, const char * argv[])
{
    int i = 0x0;
    char *ptr = NULL;
    int length = 0x0;
    
    ptr = name;
    
    while (*ptr != '\0')
    {
        length++;
        ptr = ptr + 1;
    }
    
    //point to the beginning
    ptr = name;
    
    for (i = 0; i < length; i++)
    {
        if(*ptr == 'a' || *ptr == 'e' || *ptr == 'i' || *ptr == 'o' || *ptr == 'u' )
        {
            //don't do anything. We want to keep the vouvels
            ptr= ptr + 1;
            continue;
        }
        else if (*ptr == 'A' || *ptr == 'E' || *ptr == 'I' || *ptr == 'O' || *ptr == 'U' )
        {
            //don't do anything. We want to keep the vouvels
            ptr= ptr + 1;
            continue;
        }
        else
        {
            *ptr = '\0';
            ptr = ptr+1;
        }
    }
    ptr = name;
    printf("The word with consonants only is\n ");
    for( i = 0 ; i < length ; i++)
    {
        if(*ptr != '\0')
            printf("%c",*ptr);
        ptr++;
    }
}

// Program delete vouwels in a string
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

char name[] = "Shreyas";

int main(int argc, const char * argv[])
{
    int i = 0x0;
    char *ptr = NULL;
    int length = 0x0;
    
    ptr = name;
    
    while (*ptr != '\0')
    {
        length++;
        ptr = ptr + 1;
    }
    
    //point to the beginning
    ptr = name;
    
    for (i = 0; i < length; i++)
    {
        if(*ptr == 'a' || *ptr == 'e' || *ptr == 'i' || *ptr == 'o' || *ptr == 'u' )
        {
            *ptr = '\0';
            ptr = ptr + 1;
        }
        else if (*ptr == 'A' || *ptr == 'E' || *ptr == 'I' || *ptr == 'O' || *ptr == 'U' )
        {
            *ptr = '\0';
            ptr = ptr + 1;
        }
        else
        {
            //don't do anything, just continue
            ptr = ptr+1;
            continue;
        }
    }
    ptr = name;
    printf("The word with consonants only is\n ");
    for( i = 0 ; i < length ; i++)
    {
        if(*ptr != '\0')
            printf("%c",*ptr);
        ptr++;
    }
}

// Program to determine if a given number is a perfect number
// For example 6 is a perfect number, because the factors of 6
// are 1, 2, 3 and 6 = 1+ 2+ 3. Similarly 28 too!
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char


int main(int argc, const char * argv[])
{
    int number = 0x0; //variable to check the input number
    int sum = 0x0;
    int i = 0x0;
    
    printf("Enter the number to be checked");
    scanf("%d", &number);
    
    for (i = 1; i < number; i++)
    {
        if(number%i == 0x0)
        {
            sum = sum + i;
        }
        else
        {
            continue;
        }
    }
    if (sum == number)
    {
        printf(" \n The given number %d is a perfect number", number);
    }
    else
    {
        printf(" \n The given number %d is not a perfect number", number);
    }
    return SUCCESS;
}

// Program to determine if a given number is a strong number
// A strong number is one in which the sum of factorial of digits
// is equal to the number itself for example 145 = 1! + 4! + 5!
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

int factorial(int digit)
{
    int i = 1;
    int fact = 1;
    for( i = digit ; i > 0 ; i-- )
        fact = fact * i;
    return (fact);
}

int main(int argc, const char * argv[])
{
    int number = 0x0;
    int sum = 0x0;
    int digit = 0x0;
    int original_number= 0x0;
    
    printf("\n Enter the number which you want to check for strong");
    scanf("%d", &number);
    
    original_number = number; //store the number
    
    while(number!= 0x0)
    {
        digit = number % 10;
        number = number / 10;
        sum = sum + factorial(digit);
    }
    if (sum == original_number)
    {
        printf("\n The number %d is a STRONG Number", original_number);
    }
    else
    {
        printf("\n The number %d is not a STRONG Number", original_number);
    }
    return SUCCESS;
    
}

//
//Program to print Floyd's Triangle
//Floyd's triangle is a right angled-triangle using the natural numbers.
//Examples of floyd's triangle:

//Example 1:

//1
//2 3
//4 5 6
//7 8 9 10

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char


int main(int argc, const char * argv[])
{
    int rows = 0x0;
    int i = 0x0;
    int j = 0x0;
    int var = 0x0;
    
    printf("\n Enter the number of Rows you want = ");
    scanf("%d", &rows);
    
    printf("\n The Floyds Triangle is as follows \n \n");
    
    for (i = 1; i <= rows; i++)
    {
        for (j= 1; j<= i; j++)
        {
            var++;
            printf(" %d ", var);
        }
        printf("\n");
    }
}

// Write a C program which outputs its source code
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char


int main(int argc, const char * argv[])
{
    FILE *file_ptr = NULL;
    char c ;
    
    file_ptr = fopen(__FILE__, "r");
    
    do
    {
        c= getc(file_ptr);
        putchar(c);
    }while(c != EOF);
    
    fclose(file_ptr);
    return SUCCESS;
}

// Write a C program which counts the number of digits in a number
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char


int main(int argc, const char * argv[])
{
    int number = 0x0;
    int sum= 0x0;
    
    printf("\nEnter the number = ");
    scanf("%d", &number);
    
    while(number != 0x0)
    {
        number = number/10;
        sum++;
    }
    printf("The number of digits is %d", sum);
    return SUCCESS;
}

// Write a C program which prints out the initial of a name
// For example if your name is Shreyas Ravindra it should
// print out as SR

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

char name[50] = {0};

int main(int argc, const char * argv[])
{
    char *ptr = NULL;
    printf(" Enter the long name whose initial is needed ");
    printf("\n");
    gets(name);
    
    ptr = name;
    
    printf("\n The initials are as follows :: ");
    printf("%c",*ptr);
    
    while (*ptr != '\0')
    {
        if(*ptr == ' ')
        {
            printf("%c", *(ptr+1));
        }
        ptr++;
    }
    return SUCCESS;
}

// Write a program to find the size of int without using
// sizeof operator

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char



int main(int argc, const char * argv[])
{
    int array[2] = {0x1, 0x2};
    uint8_t *ptr = NULL;
    uint8_t count = 0x0;
    
    ptr = (uint8_t*)array;
    while (*ptr != 0x2)
    {
        count++;
        ptr++;
    }
    printf("Size of Int is %d",count);
    return SUCCESS;
}

// Write a program to find the size of struct without using
// sizeof operator

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

typedef struct data {
    int roll_num;
    char name[20];
}data_t;



int main(int argc, const char * argv[])
{
    
    data_t *ptr = (data_t*)0x00000000;
    
    ptr = ptr + 1;
    printf("Size is %p", ptr);
    
    return SUCCESS;
}

//
//  main.c
//  Hello World
//
//  Created by Shreyas Ravindra on 21/06/14.
//  Working with Linked Lists in C

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char
#define ASSERT(a) while(a)



typedef struct student_data{
    int roll_num;
    struct student_data *next;
}student_t;

/********static function declarations to be used *****************/
/*****http://www.crazyforcode.com/linked-list/ *******/
static student_t* create_a_node(int data); //creates a node in a linked list
static student_t* create_a_linked_list(void); //creates a linked list
static void print_a_linked_list(student_t *ptr); //prints a linked List
static void add_node_at_end(student_t *head); // adds a new node in the list
static void add_node_in_between(student_t *head); // adds a node in between the list
static int length_of_linked_list(student_t *head); //gets the length of the linked list
static void delete_node_in_between (student_t *head); //deletes a node in position N in list
static void search_element_in_linked_list(student_t *head); //search an element in a list
static student_t* reverse_linked_list(student_t* head); //reverse a linked list
static student_t* sorted_linked_list(student_t *head); //sorting the linked list
static void delete_m_node_after_n_nodes_in_linked_list(student_t *head); //delete m nodes after n nodes
static student_t* create_a_circular_list(void); //creates a circular linked list
static void print_a_circular_linked_list(student_t *head); //prints a circular linked list
static student_t* swap_every_two_nodes_in_list(student_t *head) //swap every two nodes in a linked list


int main(int argc, const char * argv[])
{
    int choice = 0x0;
    student_t *master_list = NULL;
    student_t *circular_list = NULL;
    
    printf("*******Here is are the options for linked list******");
    printf("\n Enter the option you want");
    
    printf("\n 1. Create a LinkedList");
    printf("\n 2. Add a Node at a position N in the list");
    printf("\n 3. Delete a Node in the list");
    printf("\n 4. Sort the linked List ");
    printf("\n 5. Reverse the linked List");
    printf("\n 6. Print the linked List");
    printf("\n 7. Search for the element in the linked list");
    printf("\n 8. Add the node at the end of the linked List");
    printf("\n 9. Count the number of elements in the linked list");
    printf("\n 10.Delete M nodes after N nodes in a linked List ");
    printf("\n 11.Create a Circular Linked List");
    printf("\n 12.Find a loop in linked list");
    printf("\n 13.Swap every two nodes in a linked list");
    printf("\n 14.Move last node to the front of the linked list");
    printf("\n 15.Rotate the linked list");
    printf("\n 16.Length of the linked list");
    
    printf("\n 100. Exit");
    
    
    do
    {
        scanf("%d", &choice);
        switch (choice)
        {
            case 1:
                master_list = create_a_linked_list();
                if(master_list != NULL)
                {
                    printf("\n Linked List Successfully created");
                    printf("\n Choose option 6 to print it");
                }
                else
                {
                    printf("ERROR creating linked List");
                    ASSERT(1);
                }
                break;
                
            case 2:
                add_node_in_between(master_list);
                printf("\n Successfully added the node, press 6 in options to print");
                break;
                
            case 3:
                delete_node_in_between(master_list);
                printf("\n Successfully deleted, press 6 in options to print");
                break;
                
            case 4:
            {
                student_t *sorted_list = NULL;
                sorted_list = sorted_linked_list(master_list);
                printf("\n Sorting Successful");
                printf("\n The Sorted List is as follows --> ");
                print_a_linked_list(sorted_list);
            }
                break;
                
            case 5:
            {
                student_t *reversed_list = NULL;
                reversed_list = reverse_linked_list(master_list);
                printf("\nReverse Successful");
                printf("\n Reversed List is as follows -->");
                print_a_linked_list(reversed_list);
            }
                break;
                
            case 6:
                print_a_linked_list(master_list);
                break;
                
            case 7:
                search_element_in_linked_list(master_list);
                break;
                
            case 8:
                add_node_at_end(master_list);
                printf("\n Successfully added the node, press 6 in options to print");
                break;
            
            case 9:
                printf("\n The length of the Linked List is %d", length_of_linked_list(master_list));
                break;
            
            case 10:
                delete_m_node_after_n_nodes_in_linked_list(master_list);
                printf("\n Delete Successful, please press 6 to print the list");
                break;
            
            case 11:
                circular_list = create_a_circular_list();
                printf("\n Successfully Created a circular linked List");
                printf("\n Press 17 for printing a circular list");
                break;
            
            case 12:
                break;
            
            case 13:
                break;
            
            case 14:
                break;
            
            case 15:
                break;
            
            case 16:
                printf("\n The length of the Linked List is %d", length_of_linked_list(master_list));
                break;
            
            case 17:
                if( NULL!= circular_list)
                {
                    print_a_circular_linked_list(circular_list);
                }
                else
                {
                    printf("\n Link doesn't exist");
                }
                break;
                
            case 100:
                //exit the case
                break;
            default:
                printf("Wrong Option, please read options again ");
                
        }
        printf("\n Enter Your choice");
        
    }while (choice != 100);
    printf ("\n Good Bye, Thanks for using the program");
    
    return SUCCESS;
}


//creates a node in a linked list
static student_t* create_a_node(int data)
{
    student_t* node = NULL;
    //node = (student_t*)malloc(sizeof(struct student_data));
    //if (NULL!=node)
    //  memset(node, 0x0, sizeof(struct(student_data));
    node = (student_t*)calloc(1,sizeof(struct student_data));
    if(node != NULL)
    {
        node->roll_num = data;
        node->next = NULL;
    }
    else
    {
        printf("ERROR CREATING A NODE");
        ASSERT(1);
    }
    return node;
}

//create a linked list
static student_t* create_a_linked_list(void)
{
    student_t *ptr = NULL;
    student_t *temp = NULL;
    student_t *head = NULL;
    int i = 0x0;
    int count = 0x0;
    int data = 0x0;
    printf("\n Enter the number of enteries you want =");
    scanf("%d", &count);
    
    for (i =0 ; i< count; i++ )
    {
        printf("\n Please enter roll number %d ", i+1);
        scanf("%d", &data);
        if (i == 0x0)
        {
            //when list is empty, you need to create a list
            ptr = create_a_node(data);
            head = ptr;
        }
        else
        {
            temp = create_a_node(data);
            ptr->next = temp;
            ptr = ptr->next;
        }
    }
    return head;
}

//print a linked list
static void print_a_linked_list(student_t *ptr)
{
    student_t *temp = NULL;
    printf("\n Printing the Linked List");
    temp = ptr;
    while(temp != NULL)
    {
        printf(" %d   ",temp->roll_num);
        temp = temp->next;
    }
    return;
}

//add a node at the end of the list
static void add_node_at_end(student_t *head)
{
    student_t *new_data = NULL;
    student_t *ptr = NULL;
    int data = 0x0;
    
    ptr = head;
    printf("\n Enter the data for the new node ");
    scanf("%d", &data);
    
    new_data = create_a_node(data);
    while(ptr->next != NULL)
    {
        ptr = ptr->next;
    }
    ptr->next = new_data;
    return;
}

//add a node in position N in the list
static void add_node_in_between(student_t *head)
{
    student_t *new_data = NULL;
    student_t *temp = NULL;
    student_t *ptr = NULL;
    int data = 0x0;
    int position = 0x0;
    int count = 0x1;
    
    printf("\n Enter the data and position after which data should be written");
    scanf("%d %d",&data, &position);
    new_data = create_a_node(data);
    
    ptr = head;
    while (count != position)
    {
        ptr = ptr->next;
        count++;
    }
    temp = ptr->next; //store the original pointed address
    ptr->next = new_data;
    new_data->next = temp;
    
    return;
}
//length of the linked list
static int length_of_linked_list(student_t *head)
{
    student_t *ptr = NULL;
    int count =0x0;
    ptr = head;
    while(ptr != NULL)
    {
        ptr = ptr->next;
        count++;
    }
    return (count);
}

//delete a node in position N in the list
static void delete_node_in_between (student_t *head)
{
    student_t *ptr = NULL;
    student_t *temp = NULL;
    int position = 0x0;
    int count = 0x1;
    printf("\nEnter the node number to be deleted ie. 1st node, or 2nd etc");
    scanf("%d", &position);
    if (position > length_of_linked_list(head))
    {
        printf("ERROR in data");
        ASSERT(1);
    }
    else
    {
        ptr = head;
        while (count != position)
        {
            temp= ptr;
            ptr = ptr->next;
            count++;
        }
        temp->next = ptr->next;
        free(ptr);
    }
    printf("\n Deleted the node number %d successfully ", position);
    return;
}

// API to search an element in a linked list
static void search_element_in_linked_list(student_t *head)
{
    student_t *ptr = NULL;
    int data = 0x0;
    bool flag = false;
    
    printf("\n Enter the roll number you are looking for");
    scanf("%d", &data);
    
    ptr = head;
    
    while(ptr != NULL)
    {
        if(data == ptr->roll_num)
        {
            flag = true;
            break;
        }
        else
        {
            ptr = ptr->next;
        }
    }
    if (flag)
    {
        printf("Entered Data is FOUND in list");
    }
    else
    {
        printf("Entered Data is NOT FOUND in list");
    }
}

//reverse the linked list
static student_t* reverse_linked_list(student_t* head)
{
    student_t *ptr = NULL;
    student_t *temp = NULL;
    
    //point the ptr to head
    ptr = head;
    head = NULL;
    
    while(ptr != NULL)
    {
        temp = ptr->next;
        ptr->next = head;
        head = ptr;
        ptr = temp;
    }
    return head;
}

//sorting the linked list using bubble sort
static student_t* sorted_linked_list(student_t *head)
{
    student_t *curr_ptr= NULL;
    student_t *next_ptr= NULL;
    int temp = 0;
    int i = 0;
    int j = 0;           // temp variable to swap the data
    int length = 0x0; // length of the linked list
    
    length = length_of_linked_list(head);
    curr_ptr = head;
    next_ptr = curr_ptr->next;
    
    for(i = 0; i < length; i++)
    {
        for(j = 0; j < (length -1) ; j++)
        {
            if (next_ptr->roll_num > curr_ptr->roll_num)
            {
                temp = curr_ptr->roll_num;
                curr_ptr->roll_num = next_ptr->roll_num;
                next_ptr->roll_num = temp;
            }
            curr_ptr = curr_ptr->next;
            next_ptr = next_ptr->next;
        }
        curr_ptr = head;
        next_ptr = curr_ptr->next;
    }
    return head;
}

//delete m nodes after N nodes in a linked list
static void delete_m_node_after_n_nodes_in_linked_list(student_t *head)
{
    int m = 0x0;
    int n = 0x0;
    int count = 0x1;
    student_t *ptr = NULL;
    student_t *temp_head = NULL;
    student_t *temp = NULL;
    printf("\n Enter the number of nodes to be deleted M ");
    scanf("%d", &m);
    
    printf("\n Enter the point after which it has to be deleted N");
    scanf("%d", &n);
    
    if( n > length_of_linked_list(head) || m > length_of_linked_list(head) )
    {
        printf("ERROR in PARAMETER");
        ASSERT(1);
    }
    
    
    ptr = head;
    while (count != n)
    {
        ptr = ptr->next;
        count++;
    }
    //store this value
    //temp serves as the new head now
    temp_head = ptr->next;
    count = 0x0;
    while(count != m)
    {
        temp = temp_head->next;
        free(temp_head);
        temp_head= temp;
        count ++;
    }
    ptr->next = temp;
    return;
}

//Program to create a circular linked list
static student_t* create_a_circular_list(void)
{
    student_t *ptr = NULL;
    student_t *temp = NULL;
    student_t *head = NULL;
    int i = 0x0;
    int count = 0x0;
    int data = 0x0;
    printf("\n Enter the number of enteries you want =");
    scanf("%d", &count);
    
    for (i =0 ; i< count; i++ )
    {
        printf("\n Please enter roll number %d ", i+1);
        scanf("%d", &data);
        if (i == 0x0)
        {
            //when list is empty, you need to create a list
            ptr = create_a_node(data);
            head = ptr;
        }
        else
        {
            temp = create_a_node(data);
            ptr->next = temp;
            ptr = ptr->next;
        }
    }
    //now ptr->next should point to head
    ptr->next = head;
    return head;
}

//Program to print a circular linked list
static void print_a_circular_linked_list(student_t *head)
{
    student_t *ptr = NULL;
    printf("\nPrinting the circular linked list");
    
    ptr = head;
    do
    {
        printf(" %d  ", ptr->roll_num);
        if(ptr->next == head)
        {
            printf (" <-----> head");
        }
        ptr = ptr->next;
    }while(ptr != head);
    printf("  %d ", head->roll_num);
    return;
}

//swap every two nodes in a linked list
// 1 -> 2 -> 3 -> 4 -> 5 is input
// 2 -> 1 -> 4-> 3 -> 5
static student_t* swap_every_two_nodes_in_list(student_t *head)
{
    
}

//  Bit manupilation in C

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char
#define ASSERT(a) while(a)

static int number_of_bits_set(int *ptr);
static void print_number_in_binary(int*);
static void set_bits(int *number, int M, int N);
static void clear_bits(int *number, int M, int N);
static void toggle_bits(int *number, int M);
static bool is_number_palindrome(int *number);
static void swap_bits(int *number);


int main(int argc, const char * argv[])
{
    int number = 0x0;
    int choice = 0x0;
    int temp = 0x0;
    
    printf("\n Enter a number which needs manipulation give in hex :: 0x");
    scanf("%x", &number);
    temp = number;
    
    printf("\n Enter the choice you want to do ");
    printf("\n 1. Print the number of bits set in a number");
    printf("\n 2. Set N bits starting from M position");
    printf("\n 3. Clean N bits starting from M position");
    printf("\n 4. Toggle the bits of a number");
    printf("\n 5. Print the number in a binary form");
    printf("\n 6. Count the number of zeros in a number");
    printf("\n 7. Check if a number is binary palindrome");
    printf("\n 8. Swap the even and odd bits of a number");
    
    printf("\n Make your choice");
    scanf("%d",&choice);
    
    while (choice != 100)
    {
        switch(choice)
        {
            case 1:
                printf("\n The number of bits set in the number = 0x%x is %d ", temp, number_of_bits_set(&number));
                       break;
            case 2:
            {
                int N = 0x0;
                int M = 0x0;
                printf("\n Enter the number of bits to be set N =");
                scanf("%d", &N);
                
                printf("\n Enter the position M = ");
                scanf("%d", &M);
                
                set_bits(&number, M , N);
                print_number_in_binary(&number);
                
            }
                break;
            case 3:
            {
                int N = 0x0;
                int M = 0x0;
                printf("\n Enter the number of bits to be cleared N =");
                scanf("%d", &N);
                
                printf("\n Enter the position M = ");
                scanf("%d", &M);
                
                clear_bits(&number, M , N);
                print_number_in_binary(&number);
                
            }
                break;
                
            case 4:
            {
                int M = 0x0;
                printf ("\n Enter the bit number to be toggled = ");
                scanf("%d", &M);
                toggle_bits(&number, M);
                print_number_in_binary(&number);
            }
                break;
                
            case 5:
                printf("\n The number in binary is = 0b");
                print_number_in_binary(&number);
                break;
                
            case 6:
                printf("\n The number of bits set in the number = 0x%x is %d ", temp, 32 - number_of_bits_set(&number));
                break;
            
            case 7:
                if(is_number_palindrome(&number))
                    printf("\nThe number is a Binary PALINDROME");
                else
                    printf("\nThe number is not a Binary PALINDROME");
                break;
            
            case 8:
                printf("\n The swapped bits is as follows ");
                swap_bits(&number);
                print_number_in_binary(&number);
                printf("\nThe number in hex is 0x%x",number);
                break;
                
                
            default:
                       break;
                       
        }
        printf("\nEnter the choice you want to do");
        scanf("%d",&choice);
    }
    printf("\n Thanks for using our program ");
    return SUCCESS;
}

static void swap_bits(int *number)
{
    int local_number = 0x0;
    local_number = *number;
    local_number = (((local_number >>1 ) & 0x55555555) | ((local_number << 1) & 0xAAAAAAAA));
    *number = local_number;
}

static bool is_number_palindrome(int *number)
{
    int local_number = 0x0;
    int temp = 0x0;
    int i = 0x0;
    local_number = *number;
    
    for (i=0; i < 32 ; i++)
    {
        temp = temp |(local_number & 0x1);
        if (i == 31)
            break;
        temp = temp << 1;
        local_number = local_number >> 1;
    }
    if ( temp == *number)
    {
        return true;
    }
    else
    {
        return false;
    }
}

static void toggle_bits(int *number, int M)
{
    int local_number = 0x0;
    local_number = *number;
    local_number ^= (0x1 << M);
    *number = local_number;
    return;
}

static void clear_bits(int *number, int M, int N)
{
    int local_number = 0x0;
    int mask = 0xFFFFFFFF;
    int temp = 0x0;
    
    local_number = *number;
    set_bits(&temp, M,N);
    mask = mask ^ temp;
    local_number = local_number & mask;
    *number = local_number;
    
    return;

}

static void set_bits(int *number, int M, int N)
{
    int local_number = 0x0;
    int mask = 0x0;
    
    
    local_number = *number;
    
    while(N!= 0)
    {
        mask = mask|0x1;
        mask = mask << 0x1;
        N--;
    }
    
    mask = mask >> 0x1;
    
    mask = mask << M;
    local_number = local_number | mask;
    *number = local_number;
    
    return;
    
}
static void print_number_in_binary(int *number)
{
    int local_num = 0x0;
    int temp = 0x0;
    int i = 0x0;
    local_num = *number;
    for (i = 0; i < 32 ; i++)
    {
        temp = local_num & 0x80000000;
        temp = (temp >> 31) & 0x1;
        printf("%x",temp);
        local_num = local_num << 1;
        temp= temp^temp;
    }
    printf("\n");
}

static int number_of_bits_set(int *ptr)
{
    int number = 0x0;
    int temp = 0x0;
    int length_of_int = 0x0;
    int count = 0x0;
    number = *ptr;
    length_of_int = sizeof(int);
    length_of_int*= 8; //multiply by 8 to get in bits
    
    while(length_of_int != 0x0)
    {
        temp = number & 0x1;
        if(temp)
        {
            count++;
            temp ^= temp;
        }
        number = number >> 1;
        length_of_int --;
    }
    
    return count;
   
}//  Bit manupilation in C

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char
#define ASSERT(a) while(a)

static int number_of_bits_set(int *ptr);
static void print_number_in_binary(int*);
static void set_bits(int *number, int M, int N);
static void clear_bits(int *number, int M, int N);
static void toggle_bits(int *number, int M);
static bool is_number_palindrome(int *number);
static void swap_bits(int *number);


int main(int argc, const char * argv[])
{
    int number = 0x0;
    int choice = 0x0;
    int temp = 0x0;
    
    printf("\n Enter a number which needs manipulation give in hex :: 0x");
    scanf("%x", &number);
    temp = number;
    
    printf("\n Enter the choice you want to do ");
    printf("\n 1. Print the number of bits set in a number");
    printf("\n 2. Set N bits starting from M position");
    printf("\n 3. Clean N bits starting from M position");
    printf("\n 4. Toggle the bits of a number");
    printf("\n 5. Print the number in a binary form");
    printf("\n 6. Count the number of zeros in a number");
    printf("\n 7. Check if a number is binary palindrome");
    printf("\n 8. Swap the even and odd bits of a number");
    
    printf("\n Make your choice");
    scanf("%d",&choice);
    
    while (choice != 100)
    {
        switch(choice)
        {
            case 1:
                printf("\n The number of bits set in the number = 0x%x is %d ", temp, number_of_bits_set(&number));
                       break;
            case 2:
            {
                int N = 0x0;
                int M = 0x0;
                printf("\n Enter the number of bits to be set N =");
                scanf("%d", &N);
                
                printf("\n Enter the position M = ");
                scanf("%d", &M);
                
                set_bits(&number, M , N);
                print_number_in_binary(&number);
                
            }
                break;
            case 3:
            {
                int N = 0x0;
                int M = 0x0;
                printf("\n Enter the number of bits to be cleared N =");
                scanf("%d", &N);
                
                printf("\n Enter the position M = ");
                scanf("%d", &M);
                
                clear_bits(&number, M , N);
                print_number_in_binary(&number);
                
            }
                break;
                
            case 4:
            {
                int M = 0x0;
                printf ("\n Enter the bit number to be toggled = ");
                scanf("%d", &M);
                toggle_bits(&number, M);
                print_number_in_binary(&number);
            }
                break;
                
            case 5:
                printf("\n The number in binary is = 0b");
                print_number_in_binary(&number);
                break;
                
            case 6:
                printf("\n The number of bits set in the number = 0x%x is %d ", temp, 32 - number_of_bits_set(&number));
                break;
            
            case 7:
                if(is_number_palindrome(&number))
                    printf("\nThe number is a Binary PALINDROME");
                else
                    printf("\nThe number is not a Binary PALINDROME");
                break;
            
            case 8:
                printf("\n The swapped bits is as follows ");
                swap_bits(&number);
                print_number_in_binary(&number);
                printf("\nThe number in hex is 0x%x",number);
                break;
                
                
            default:
                       break;
                       
        }
        printf("\nEnter the choice you want to do");
        scanf("%d",&choice);
    }
    printf("\n Thanks for using our program ");
    return SUCCESS;
}

static void swap_bits(int *number)
{
    int local_number = 0x0;
    local_number = *number;
    local_number = (((local_number >>1 ) & 0x55555555) | ((local_number << 1) & 0xAAAAAAAA));
    *number = local_number;
}

static bool is_number_palindrome(int *number)
{
    int local_number = 0x0;
    int temp = 0x0;
    int i = 0x0;
    local_number = *number;
    
    for (i=0; i < 32 ; i++)
    {
        temp = temp |(local_number & 0x1);
        if (i == 31)
            break;
        temp = temp << 1;
        local_number = local_number >> 1;
    }
    if ( temp == *number)
    {
        return true;
    }
    else
    {
        return false;
    }
}

static void toggle_bits(int *number, int M)
{
    int local_number = 0x0;
    local_number = *number;
    local_number ^= (0x1 << M);
    *number = local_number;
    return;
}

static void clear_bits(int *number, int M, int N)
{
    int local_number = 0x0;
    int mask = 0xFFFFFFFF;
    int temp = 0x0;
    
    local_number = *number;
    set_bits(&temp, M,N);
    mask = mask ^ temp;
    local_number = local_number & mask;
    *number = local_number;
    
    return;

}

static void set_bits(int *number, int M, int N)
{
    int local_number = 0x0;
    int mask = 0x0;
    
    
    local_number = *number;
    
    while(N!= 0)
    {
        mask = mask|0x1;
        mask = mask << 0x1;
        N--;
    }
    
    mask = mask >> 0x1;
    
    mask = mask << M;
    local_number = local_number | mask;
    *number = local_number;
    
    return;
    
}
static void print_number_in_binary(int *number)
{
    int local_num = 0x0;
    int temp = 0x0;
    int i = 0x0;
    local_num = *number;
    for (i = 0; i < 32 ; i++)
    {
        temp = local_num & 0x80000000;
        temp = (temp >> 31) & 0x1;
        printf("%x",temp);
        local_num = local_num << 1;
        temp= temp^temp;
    }
    printf("\n");
}

static int number_of_bits_set(int *ptr)
{
    int number = 0x0;
    int temp = 0x0;
    int length_of_int = 0x0;
    int count = 0x0;
    number = *ptr;
    length_of_int = sizeof(int);
    length_of_int*= 8; //multiply by 8 to get in bits
    
    while(length_of_int != 0x0)
    {
        temp = number & 0x1;
        if(temp)
        {
            count++;
            temp ^= temp;
        }
        number = number >> 1;
        length_of_int --;
    }
    
    return count;
   
}

//  Program to dynamically allocate an array

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char
#define ASSERT(a) while(a)




int main(int argc, const char * argv[])
{
    int n = 0x0;
    int i =0;
    int *array = NULL;
    printf("\n Enter the size of the array");
    scanf("%d", &n);
    array = (int*)malloc(n*sizeof(int));
    if(NULL != array)
    {
        for(i = 0; i<n; i++)
        {
            array[i] = i*5;
        }
    }
    else
    {
        ASSERT(0x1);
    }
    printf("The entered array is = \n");
    for (i=0 ;i<n ; i++)
    {
        printf("%d \t ", *(array+i));
    }
    return 0;
}

//  Program to get a 16 byte aligned memory

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <inttypes.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char
#define ASSERT(a) while(a)



int main(int argc, const char * argv[])
{
    void *ptr = NULL;
    uintptr_t new_ptr;
    ptr = malloc(1024+16);
    if(NULL == ptr)
        ASSERT(1);
    new_ptr = ((uintptr_t)ptr+15) & ~(uintptr_t)0x0F;
    printf("0x%" PRIXPTR,(uintptr_t)ptr);
    
    printf("\n");
    printf("16Byte Aligned Memory is= ");
    printf("0x%" PRIXPTR,(uintptr_t)new_ptr);
    
    return 0;
    
}


//Program to work with binary trees!
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

//typical node in a binary tree
typedef struct data_t
{
  int data;
  struct data_t *left;
  struct data_t *right;
}data;

//creating the node for Binary Tree
static data* create_node(void)
{
  int new_data = 0x0;
  data *ptr = NULL;
  printf("\nEnter the data you want to store(-1 to store nothing)\n");
  scanf("%d", &new_data);
  if(-1 == new_data)
  {
    return NULL;
  }
  
  ptr = (data*)malloc(sizeof(data));
  ptr->data=new_data;
  
  printf("\n Enter the left node of %d",new_data);
  ptr->left = create_node();
  
  printf("\n Enter the right node value of %d",new_data);
  ptr->right = create_node();
  
  return ptr;
  
}

//Pre-Order traversal
static void pre_order(data* root)
{
  if(root != NULL)
  {
    printf("%d  ",root->data);
    //traverse the left tree first
    pre_order(root->left);
    //traverse the right tree next
    pre_order(root->right);
  }
}

//Post Order Traversal
static void post_order(data* root)
{
  if(root != NULL)
  {
    //traverse the left tree first
    post_order(root->left);
    //traverse the right tree next
    post_order(root->right);
    printf("%d  ", root->data);
  }
}

//program to look up data in a binary tree
static bool look_up_data(data* root, int search_data)
{
  if (NULL == root)
  {
    return false;
  }
  else
  {
    if(search_data == root->data)
    {
      return true;
    }
    else if (true == look_up_data(root->left,search_data))
    {
      return true;
    }
    else if (true == look_up_data(root->right,search_data))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}

//creating a new node dynamically
static data* new_node(int value)
{
  data* ptr = NULL;
  ptr = (data*)malloc(sizeof(data));
  if(ptr!= NULL)
  {
    ptr->data = value;
    ptr->left = NULL;
    ptr->right = NULL;
  }
  return ptr;
}

//inserting a node in a binary tree
static data* insert_node(data* root, int value)
{
  if(NULL == root)
  {
    return(new_node(value));
  }
  else
  {
    if(value < root->data)
    {
      root->left = insert_node(root->left, value);
    }
    else
    {
      root->right = insert_node(root->right,value);
    }
  }
  return root;
}

//count the number of nodes
static int count_number_of_nodes(data* root)
{
  int count = 1; //first one is always the ROOT.
  if(root == NULL)
  {
    return 0x0;
  }
  else
  {
    count += count_number_of_nodes(root->left);
    count += count_number_of_nodes(root->right);
  }
  return count;
}

//minimum value in the nodes
static int min_value_of_nodes(data* root)
{
  int min;
  int left;
  int right;
  int root_data;
  if(root != NULL)
  {
    root_data = root->data;
    left = min_value_of_nodes(root->left);
    right = min_value_of_nodes(root->right);
    if(left < right)
    {
      min = left;
    }
    else
    {
      min = right;
    }
    if(min > root_data)
    {
      min = root_data;
    }
  }
  return min;
}

//Depth of a binary tree
static int depth_of_tree(data* root)
{
  if(NULL == root)
  {
    return 0x0;
  }
  else
  {
    int ldepth = depth_of_tree(root->left);
    int rdepth = depth_of_tree(root->right);
    
    if (ldepth > rdepth)
    {
      return (ldepth+1);
    }
    else
    {
      return(rdepth+1); 
    }
  }
}

//Mirror of the binary tree
static void mirror_binary_tree(data* root)
{
  if(NULL == root)
  {
    return;
  }
  else
  {
    data* temp;
    mirror_binary_tree(root->left);
    mirror_binary_tree(root->right);
    
    //swap the nodes
    temp = root->left;
    root->left = root->right;
    root->right = temp;
  
  }
}

//Main Program. This is where the menu has to be entered
int main(int argc, const char * argv[])
{
  data *master_root = NULL;
  int choice =0x0;
  master_root = create_node();
  
Options:
  printf("\nPlease read the options below and choose what you want to do:");
  printf("\n1. Pre-Order traversal");
  printf("\n2. Look Up for data");
  printf("\n3. Insert a Node");
  printf("\n4. Count the nodes in a binary tree");
  printf("\n5. Minimum Value in a binary tree");
  printf("\n6. Depth of a binary tree");
  printf("\n7. Post-Order traversal");
  printf("\n8. Mirror the Binary tree");
  printf("\n100. Exit");
  printf("\n Enter your choice");
  scanf("%d", &choice);
  
  switch (choice)
  {
    case 1:
    {
      if(master_root != NULL)
      {
        pre_order(master_root);
      }
    }
    break;
    
    case 2:
    {
      int search_var = 0x0;
      printf("\nEnter the data you are looking for");
      scanf("%d", &search_var);
      
      if (NULL!= master_root)
      {
        if(true == look_up_data(master_root, search_var))
          printf("\n DATA FOUND");
        else
          printf("\n DATA not FOUND");
      }
  
    }
    break;
    
    case 3:
    {
      int new_data = 0x0;
      printf("\nEnter the data to be inserted");
      scanf("%d", &new_data);
      
      if(NULL != insert_node(master_root, new_data))
      {
        printf("\n Data added successfully, print to check!! ");
      }
      else
      {
        while(FAIL);
      }
    }
    break;
      
    case 4:
    {
      if(NULL != master_root)
        printf("\n The number of nodes is %d",count_number_of_nodes(master_root));
    }
    break;
      
    case 5:
    {
      if(NULL != master_root)
        printf("\n The minimum value in a BT is %d", min_value_of_nodes(master_root));
    }
    break;
      
    case 6:
    {
      if(NULL != master_root)
      {
        printf("The depth of the BT is %d", depth_of_tree(master_root));
      }
    }
    break;
      
    case 7:
    {
      if(master_root != NULL)
      {
        pre_order(master_root);
      }
    }
    break;
      
    case 8:
    {
      if(NULL != master_root)
      {
        pre_order(master_root);
        printf("\n Mirroring the binary tree");
        mirror_binary_tree(master_root);
        printf("\n Successfully mirrored \n");
        pre_order(master_root);
      }
    }
    break;
      
    case 100:
      goto END;
                                                       
    default:
      printf("Wrong Choice");
      goto Options;
  }
  goto Options;
 
END:
  return SUCCESS;
}


//Program to have printf in a Macro
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#define SUCCESS 0x0
#define FAIL 0xDEADBEAF
#define UINT8 char

#define MYPRINT(...) printf(__VA_ARGS__)

int main(void)
{
    MYPRINT("Hello World \n");
    MYPRINT("%lu", sizeof(int));
    return SUCCESS;
}

/* Data types in C */ 
/*
Data Type             Memory (bytes)          Range                      Format Specifier
short int                   2          -32,768 to 32,767                       %hd
unsigned short int          2           0 to 65,535                            %hu
unsigned int                4           0 to 4,294,967,295                     %u
int                         4          -2,147,483,648 to 2,147,483,647         %d
long int                    4          -2,147,483,648 to 2,147,483,647         %ld
unsigned long int           4           0 to 4,294,967,295                     %lu
long long int               8          -(2^63) to (2^63)-1                     %lld
unsigned long long int      8           0 to 18,446,744,073,709,551,615        %llu
signed char                 1          -128 to 127                             %c 
unsigned char               1           0 to 255                               %c
float                       4                                                  %f
double                      8                                                  %lf
long double                 12                                                 %Lf
*/

//Program to find the offset of an element in a structure

#include <stdio.h>
#include <stdlib.h>
#define CRASH() while(1)

#define OFFSET_OFF(TYPE, ELEMENT) ((size_t)&(((TYPE*)0)->ELEMENT))
typedef struct student_t 
{
	int i; 
	double d; 
	char c; 
}student; 
 
int main()
{
	printf("%lu", OFFSET_OFF(student, c)); 

	printf("\n"); 

	return 0x0; 	
}

//Program to play with strings

#include <stdio.h>
#include <stdlib.h>

#define SIZE 10 


// this is possible as it returns the variable from ROM
char* get_rom_string(void)
{
	char* rom_string = "ROM_Variable"; 
	return rom_string; 
}

//this is possible as it returns data from HEAP 
char* get_heap_string(void)
{
	char* heap_string = (char*)malloc(10 * sizeof(char)); 
	if(NULL!=heap_string)
	{
		heap_string="Heap_string"; 
	}
	return heap_string;
}

//this is not possible as there would be a stack windup
// there will be a warning only not a compilation error
char* get_stack_string(void)
{
	char stack_string[] = "Stack_string"; 
	return stack_string; 
}




int main(void)
{
	char * name; 
	char name1[] = "Sahil"; 
	name = "Shreyas"; 
	// *(name+1) ='u'; This is not allowed as this is stored in RO section 
	*(name1+1) = 'p';
	printf("\n %s",name); 
	printf("\n %s",name1);

	printf("\n playing with strings "); 
	printf("\n %s ",get_rom_string()); 
	printf("\n %s", get_heap_string());
	printf("\n %s", get_stack_string());  

	printf("\n"); 

	return 0x0; 
}

//Program to overload a function in C without using #DEF

#include <stdio.h>
#include <stdlib.h>

typedef enum type_t
{
	INT = 0, 
	CHAR = 1, 
}my_type; 

void print_data_type(void*, int); 

int main(void)
{
	int data = 0x10; 
	char letter = 'S'; 
	int* var = &data; 
	char* text = &letter; 

	printf ("\n"); 

	printf("\n Printing data elements "); 
	print_data_type(var, INT); 
	print_data_type(text, CHAR); 

	printf ("\n"); 
	return 0x0;  
}

void print_data_type(void* param, int type)
{
	switch(type)
	{
		case INT: 
			{
				printf("\n 0x%x", *((int*)param));
			}
		break; 
		case CHAR:
			{
				printf("\n %c", *((char*)param));
			}
		break; 
		default: 
			printf(" \n ERROR!!! ");  
	}
}

//Program to work with function pointers

#include <stdio.h>
#include <stdlib.h>

//function pointer which takes a parameter int and returns a pointer
char* (* hello_world)(int); 

char* new_hello_world (int count)
{
	char *array = "Shreyas"; 

	while(count != 0x0)
	{
		printf ("\n Hello World \n "); 
		count --; 
	}
	return array; 
}

int main(void)
{
	hello_world = new_hello_world; 
	printf("%s", hello_world(0x5)); 

	printf("\n"); 

	return 0x0; 
}

//Program to work with union pointers

#include <stdio.h>
#include <stdlib.h>

typedef union my_union_tag
{
    int v1;
    int array[5];
}union_t;

int main(void)
{
    int count;
    union_t var;
    typedef char byte;
    byte * ptr = NULL;
    printf("%lu \n ",sizeof(union_t));
    count = sizeof(union_t);
    ptr = &var;
    while(count != 0 )
        {
        *ptr = 0x1 + 1;
        count --;
        ptr++;
        }
    printf("\n Printing the union ");
    ptr = &var;
    for(count=0 ; count != sizeof(union_t); count++)
        {
        printf("\n 0x%x",*ptr); 
        ptr++; 
        
        }
    
    ptr= NULL; 
    
    return 0x0;
}

//program to find the Missing Number in an array 

#include <stdio.h>
#include <stdlib.h> 


int main(void)
{
	int array[]= {1,3,4,5,6}; 
	int x1 = 0x0; 
	int x2 = 0x1; 
	int i = 0x0; 
	int n = sizeof(array)/sizeof(array[0]); 

	x1 = array[0]; 

	for(i= 2; i<=n; i++)
	{
		x2 = x2 ^ i;
	}

	for (i = 1; i<(n-1); i++ )
	{
		x1 = x1 ^ array[i]; 
	}	

	printf("missing number = %d ", (x1^x2)); 

	return 0x0;
}

//program to do a aligned malloc

#include <stdio.h>
#include <stdlib.h>

void* malloc_alligned(size_t size, size_t alignment) 
{
	void *p1; 
	void *p2; 
	size_t addr; 

	//Allocate the size + alignment 
	p1 = (void*)malloc(size+alignment);

	addr = (size_t)p1 + alignment; 

	p2 = (void*)(p1 - (addr % alignment));

	return p2;  
}
int main(void)
{
	int *p = (int*)malloc_alligned(10,4096);
	printf("pointer is %p \n", p );
	return 0x0; 
}

//program to find the offset of an element in a structure

#include <stdio.h>
#include <stdlib.h>

typedef struct student_t 
{
	int i; 
	double d; 
	char c; 
}student; 

int main(void)
{
	student *ptr = (student*)0x0; 
	printf("Offset of i is %p\n", &(ptr->i)); 

	return 0x0;  
}

//program to find if a given number is a sparse or not
//in a sparse number there will be no bits which are consecutively
//set. For eg 72 = 0b 01001000

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int number = 0x0;
    int temp = 0x0;
    int mask = 0x3; //0b11
    int count = sizeof(int)* 8; //32
    
    printf("\nEnter a number to check");
    scanf("%d",&number);
    
    temp = number;
    
    while(count != 0)
    {
        if((number & mask) == 0x3)
        {
            //program is complete if this condition is met
            printf("\nThe given number %d is not a SPARSE number", temp);
            return 0x0;
        }
        else
        {
            //right shift the number by 2 bits
            number = number >> 2;
            count = count - 2;
        }
    
    }
    printf("The given number = %d is a SPARSE number \n", temp);
    return 0x0;
}

//program to swap two nibbles in a byte 

#include <stdio.h>
#include <stdlib.h>

typedef char uint8; 

int main(void)
{
    uint8 number = 0x0; 
    uint8 temp1 = 0x0; 
    uint8 temp2 = 0x0;
   
    printf("\nEnter a 8 bit number"); 
    scanf("%c", &number); 

    printf("\n Original Number is = 0x%x",number ); 

    temp1 = number & 0xF; //get the lower nibble
    temp2 = number & 0xF0; 
    number ^= number; 
    number |= (temp2 >> 4); 
    number |= (temp1 << 4); 

    printf("The Swapped number is 0x%x ",number );
    return 0x0; 

}

//program to find the next higher number with same bits

#include <stdio.h>
#include <stdlib.h>

int get_set_bits(int number)
{
	int temp = 0x0;  
	int i;
	int count = 0x0;  

	for(i =0; i< (sizeof(int)*8); i++)
	{
		temp = number & 0x1; 
		if(temp == 1)
			count++;
		number = number >> 0x1; 

	}
	return count; 
}

int main(void)
{
    int number = 0x0; 
    int number_of_bits_set = 0x0; 

    printf("\nEnter the number"); 
    scanf("%d",&number); 

    number_of_bits_set = get_set_bits(number); 

    while(1)
    {
    	number++; 
    	if(get_set_bits(number) == number_of_bits_set)
    		break; 
    }
    printf("The next higher number with same number of bits set is %d\n",number); 

    return 0x0; 
}

//program to find the next higher number with same bits

#include <stdio.h>
#include <stdlib.h>

int get_set_bits(int number)
{
	int temp = 0x0;  
	int i;
	int count = 0x0;  

	for(i =0; i< (sizeof(int)*8); i++)
	{
		temp = number & 0x1; 
		if(temp == 1)
			count++;
		number = number >> 0x1; 

	}
	return count; 
}

int main(void)
{
    int number = 0x0; 
    int number_of_bits_set = 0x0; 

    printf("\nEnter the number"); 
    scanf("%d",&number); 

    number_of_bits_set = get_set_bits(number); 

    while(1)
    {
    	number++; 
    	if(get_set_bits(number) == number_of_bits_set)
    		break; 
    }
    printf("The next higher number with same number of bits set is %d\n",number); 

    return 0x0; 
}

//program to work with structures with bits

#include <stdio.h>
#include <stdlib.h>

typedef struct date_tag
{
	int date : 5; 
	int month : 4; 
	int year; 
}date; 

int main(void)
{
	date object; 
	date* ptr = (date*)0x0; 
	printf("The size of structure is %lu", sizeof(object));

	printf(".....Printing offsets....\n"); 

	//note that you cannot get the address of the bits 
	//you will get compilation error

	//printf("%p", &ptr->date); 
	//printf("\t %p", &ptr->month); 

	printf("\t %p", &ptr->year); 

	printf("\n");

	return 0x0;  
}
//write a program to check whether a number is a power of 4
#include<stdio.h>
#include<stdbool.h>

static bool is_power_of_four(int num)
{
    int count = 0; 
    int i = 0; 
    int temp = 0; 
    //check if only one bit is set in the number 
    if((num & (num-1)) != 0x0)
    {
        return false; 
    }
    else
    {
        for(i=0; i<= 31; i++)
        {
            temp = num & 0x1; 
            if(temp == 0x0)
            {
                count++;
                num = num >> 1;  
            }
            else
            {
                break; 
            }
        }
        if(count%2 == 0)
        {
            return true; 
        }
        else
        {
            return false; 
        }
    }
}

int main(void) 
{
    int number = 64; 
    if(is_power_of_four(number))
    {
        printf("\n Yes %d is a power of FOUR",number); 
    }
    else
    {
        printf("\n No it is not a power of FOUR"); 
    }
    return 0x0; 
}

//Write a program to check if a number is a binary palindrome

#include<stdio.h>
#include<math.h>
#include<stdbool.h>

static array[32] = {0}; 

static int highest_bit_set(int number)
{
    int count = 31; 
    int temp = 0x0; 
    int mask = 0x80000000;
   int i = 0; 
    for(i=0; i < (sizeof(int)*8); i++)
    {
        temp = number & mask;
        if(temp)
        {
            return count; 
        } 
        temp = 0x0;
        mask = mask >> 0x1; 
        count --; 
    } 
    return -1; 
}
static bool is_palindrome(int number)
{
    int msb_set = 0x0; 
    int *start_ptr = NULL; 
    int *end_ptr = NULL; 
    int i = 0x0;

    msb_set = highest_bit_set(number); 
    // Store the numbers in array
    for(i=0; i < (sizeof(int)*8); i++)
    {
        array[i] = number & 0x1; 
        number = number >> 0x1; 
    } 
    
    start_ptr = array; 
    end_ptr = array + msb_set;
    
    for(i=0; i<=(msb_set/2); i++)
    {
        if(*start_ptr != *end_ptr)
        {
            return false; 
        }
        start_ptr++; 
        end_ptr--;
    }
    return true; 
}
int main(void)
{
    int variable = 129; 
    if(variable == 0x1 || variable == 0x0)
    {
        printf("\n The number is not eligible for a palindrome check");
    } 
    else if(is_palindrome(variable))
    {
        printf("\n The number is a binary palindrome");
    }
    else
    {
        printf(" The number is NOT a binary palindrome"); 
    } 
    return 0x0;
}

//Write a program to find the number occurring odd number of times in an array
#include<stdio.h>

int array[11] = {1,2,3,1,2,3,5,1,1,2,2}; 


int main(void) 
{
    int i=0; 
    int res =0; 

    for(i=0; i<(sizeof(array)/sizeof(array[0])); i++)
    {
        res=res ^ array[i]; 
    }
    printf("The number which occurs odd number of times in an array is %d", res); 
    return 0x0; 
}

//Write a Program to determine the Endianess of a System
#include <stdio.h>

int main(void)
{
    unsigned int number = 0x1; 
    char* ptr = (char*)&number; 

    if(*ptr)
        printf("\n Little Endian"); 
    else
        printf("\n Big Endian"); 

    return 0x0; 
}

//Write a program to count the number of bits that needs to be flipped to convert
//a number A to B 
#include <stdio.h>


static int bits_to_set(int a, int b)
{
    int result = 0;
    int temp = 0x0; 
    int i = 0x0; 
    int count = 0x0; 

    //find XOR(A,B)
    result = a ^ b;

    //count the number of bits set in result
    for(i=0; i<(sizeof(int)*8); i++)
    {
        temp = result & 0x1; 
        if(temp)
        {
            count++; 
        }
        result >>= 0x1; 
        temp = 0x0; 
    } 
    return count; 
}

int main(void)
{
    int num_1 = 0x5;
    int num_2 = 0x6;

    printf("The number of bits which needs to be set is %d", bits_to_set(num_1,num_2)); 
    return 0x0; 
}

//Write a program to find the next Power of 2 given a number N
//for example if i/p is 3 answer should be 4. 

#include<stdio.h>

static int highest_bit_set(int number)
{
    int count = 31; 
    int temp = 0x0; 
    int mask = 0x80000000;
    int i = 0; 
    for(i=0; i < (sizeof(int)*8); i++)
    {
        temp = number & mask;
        if(temp)
        {
            return count; 
        } 
        temp = 0x0;
        mask = mask >> 0x1; 
        count --; 
    } 
    return -1; 
}

int main(void)
{
    int ip_number = 17; 
    int temp = 0x0; 
    int max_bit_set = 0x0; 
    int mask = 0x1; 
    
    max_bit_set = highest_bit_set(ip_number); 

    //next power of 2  would be max_bit_set +1 
    max_bit_set++; 

    mask = mask << max_bit_set; 

    ip_number = ip_number | mask; 
    ip_number = ip_number & mask; 

    printf("The next power of 2 is %d", ip_number); 
    
    return 0x0; 
}

//Write a program to find the missing number in an array which has "N"
//Contiguous numbers 

#include <stdio.h> 

int array[10] ={1,2,3,4,6,7,8,9};

int main(void)
{
    int size = 0x0; 
    int sum = 0x0; 
    int sum_of_array = 0x0; 
    int i = 0x0; 

    size = sizeof(array)/sizeof(int); 
    size = size -1; 

    sum = size*(size +1)/2; 

    for(i =0; i<(sizeof(array)/sizeof(int)); i++)
    {
        sum_of_array += array[i];
    }

    printf("Missing Number is %d", (sum - sum_of_array)); 
    return 0x0; 
}

//Program to print the ASCII value of all the letters 
#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    char var = 'a'; 
    int i =0x0; 
    char cap = 'A'; 
    
    printf("\n Printing ASCII values from a-z\n ");
    
    for(i =0 ; i < 26 ; i++)
    {
        printf("%d\n",var);
        var++; 
    }
    
    printf("\n Printing ASCII values from A-Z\n ");
    
    for(i =0 ; i < 26 ; i++)
    {
        printf("%d\n",cap);
        cap++; 
    }
    return 0x0; 
}

//Program to find whether two strings are Anagrams
//Assume strings are written in lower case 
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

char string_1[20] = "baat"; 
char string_2[20] = "bat";

static bool is_anagram(char* str1, char* str2)
{
    bool flag = true; 
    int i = 0;
    int j = 0;
    char temp; 
    
    //compare for string lengths first 
    if(strlen(str2) > strlen(str1))
    {
        return false; 
    }
    else if (strlen(str2) <= strlen(str1))
    {
        for(i=0; i<strlen(str1); i++)
        {
            temp = *(str1+i); 
            for(j=0; j<strlen(str2); j++)
            {
                if(temp == *(str2+j))
                {
                    break; 
                }
                else
                {
                    continue;
                }
            }
            //if for any instance j == strlen(str2)
            //means that the character is not there
            if(j == strlen(str2))
            {
                flag = false; 
                break; 
            }
        }
    }
    if(flag == true)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int main(void)
{
    if(is_anagram(string_1, string_2))
    {
        printf("\n Yes the strings ARE anagrams");
    }
    else
    {
        printf("\n They are NOT anagrams"); 
    }
}

//Write a program to find out if there is a pythagorean triplet in a given array
#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int i, j, k; 
    int x, y, z; 
    int array[10] = {7,8,9,10,11,12,13,1,2,3};
    
    for(i=0; i<10; i++)
    {
        for(j=i+1; j<10; j++)
        {
            for(k=j+1; k<10; k++)
            {
                x = array[i]*array[i]; 
                y = array[j]*array[j]; 
                z = array[k]*array[k];
                if(x==y+z || y==x+z || z==x+y)
                {
                    printf("YES there is a pythagorean triplet in array");
                    goto END; 
                }
            }
        }
    }
    printf("NO there is no pythagorean triplet in the array"); 
    END:
    return 0x0; 
}

//Program to find a square of a number without using the multiplication 
//operation
#include <stdio.h>

int main() 
{
    int number =0x0; 
    int square_of_number = 0x0; 
    int i =0x0;
    int j = 0x1; 
    
    printf("Enter the number whose square is to be found \n"); 
    scanf("%d",&number); 
    
    while(i< number)
    {
        square_of_number = square_of_number + j; 
        i=i+1; 
        j=j+2; 
    }
    
    printf("The square of a number %d is %d",number, square_of_number);
	return 0;
}

//Program to find a square root  of a number without using sqrt() function

#include <stdio.h>
#define LOOP_COUNT 1000

int main() 
{
    int number =0x0; 
    float sqrt_of_num = 0x1;
    int i = 0x0; 
    int prev = 0x0; 
    
    printf("Enter the number whose square root is to be found \n");
    scanf("%d",&number);
    
    for(i=0; i<LOOP_COUNT; i++)
    {
        prev = sqrt_of_num;
        sqrt_of_num = (sqrt_of_num + number/sqrt_of_num)/2; 
        if(prev == sqrt_of_num)
        {
            break;
        }
    }
    printf("Square root of %d, is %f", number, sqrt_of_num);
    return 0x0; 
}

//Write a program to prove Goldbach's Theory! 
//Every even number greater than 4 can be expressed as a sum of two 
//prime numbers. 
//i/p = 6 o/p = 3,3 
//i/p = 8 o/p = 5, 3
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

static bool is_prime(int num)
{
    int i=0x0; 
    bool flag = true; 
    
    if(num == 0x1)
    {
        return false; 
    }
    else 
    {
        for(i= 2; i < num; i++)
        {
            if(num % i == 0x0)
            {
                flag = false; 
                break; 
            }
        }
    }
    return(flag); 
}

static void fill_prime_array(int number, int* array_ptr)
{
    int i =0x0; 
    while(i != number)
    {
        if(is_prime(i))
        {
            //store the prime numbers in array
            *array_ptr = i; 
            array_ptr++; 
        }
        i++;
    }
    return; 
}

static void find_two_numbers(int* array, int* a, int* b, int sum)
{
    int i = 0x0;
    int j = 0x0; 
    int count = sum; 
    for(i= 0; i<count; i++)
    {
        for(j=0;j<count; j++)
        {
            if(sum == *(array+i) + *(array+j))
            {
                //Store the numbers
                *a = *(array+i); 
                *b = *(array+j); 
                break; 
            }
        }
    }
}

int main(void)
{
    int number = 0x0; 
    int *prime_array = NULL; 
    int num1 = 0x0; // number 1 
    int num2 = 0x0; // number 2 
    
    printf("Enter the even number = "); 
    scanf("%d",&number); 
    if(number <= 0x4)
    {
        printf("\n Invalid number! Program will exit");
        goto END; 
    }
    //dynamically allocate memory for the array
    prime_array = (int*) calloc(number,sizeof(int)); 
    if(NULL == prime_array)
    {
        printf("\n Unable to allocate array! Program will exit"); 
        goto END; 
    }
    //memset the allocated memory to zero
    memset(prime_array, 0x0, number*sizeof(int)); 
    
    //Find the prime numbers till the given number and store in the
    //prime_array
    fill_prime_array(number, prime_array); 
    
    //Now find the two numbers
    find_two_numbers(prime_array, &num1, &num2, number); 
    
    printf("\n %d = %d + %d",number, num1, num2);
    
END:
    return 0x0; 
}

//Program to know the size of structure when bit fields are used 
#include<stdio.h>

struct temp1 
{
    char a:1; 
    char b:2;
    char c:3; 
}obj_temp1; 

struct temp2 
{
    char a;
    char b; 
    char c;
}obj_temp2; 

int main(void)
{
    printf("\n The size of temp1 = %lu", sizeof(obj_temp1)); 
    printf("\n The size of temp2 = %lu", sizeof(obj_temp2)); 
    return 0x0; 
}

//Program to multiply a number by 7 without using the "*" operator
#include<stdio.h>

int main(void)
{
    int number = 0x0; 
    int result = 0x0;
    
    printf("\n Enter the number:");
    scanf("%d",&number);
    
    //multiply the number by 8 and then subtract
    result = ((number << 3) - number); 
    
    printf ("\n %d * 7 = %d",number,result );
    
    return(0x0);
}

//Program to convert a binary number into a decimal number 
#include <stdio.h>

int main(void)
{
    int binary,decimal=0x0,digit; 
    int j =1; 
    
    printf("\n Enter the number in binary = "); 
    scanf("%d", &binary); 
    
    while(binary != 0x0)
    {
        digit = binary % 10; 
        decimal = decimal + digit*j; 
        j*=2; 
        binary = binary/10;
    }
    printf("\n The number in decimal = %d",decimal);
    return 0x0; 
}

//Program to find the position of the right most bit set
#include<stdio.h> 
#define MASK 0x1

int main(void)
{
    int number =0x0; 
    int position = 0x0; 
    int i=0x0;
    
    printf("\n Enter the number"); 
    scanf("%d", &number); 
    
    for(i=0; i<(sizeof(int)*8); i++)
    {
        if(number & MASK)
        {
            break;
        }
        else
        {
            number = number >> 1; 
            position++; 
        }
    }
    printf("\nThe right most bit set is = %d",position);
    return 0x0;
}

//Program with 2-D array 
#include<stdio.h>

int array[2][3] = {0}; 

int main(void)
{
    int i=0; 
    int j=0; 


    printf("\n Enter the array elements ::"); 
    
    for(i=0; i<2; i++)
    {
        for(j=0; j<3; j++)
        {
            scanf("%d",(*(array+i)+j));
        }
    }

    printf("\n The array elements entered are ==>"); 
    printf("\n");
    for(i=0; i<2; i++)
    {
        for(j=0; j<3; j++)
        {
            printf("%d  ",*(*(array+i)+j));
        }
        printf("\n");
    }
    return 0x0; 

}

//prorgram to find the sum of two
//2-D arrays

#include<stdio.h>
#define MAX_ROWS        0x3 
#define MAX_COLUMNS     0x3

int main(void)
{
    int a[MAX_ROWS][MAX_COLUMNS] = {0}; 
    int b[MAX_ROWS][MAX_COLUMNS] = {0}; 
    int c[MAX_ROWS][MAX_COLUMNS] = {0}; 
    int i = 0; 
    int j = 0;

    /*Create some random array*/
    for(i=0;i<MAX_ROWS;i++)
    {
        for(j=0; j<MAX_COLUMNS; j++)
        {
            *(*(a+i)+j) = i*2 + j+2; 
            *(*(b+i)+j) = j*2; 
        }
    }

    printf("\nArray a = \n"); 
    for(i=0;i<MAX_ROWS;i++)
    {
        for(j=0; j<MAX_COLUMNS; j++)
        {
            printf("%d  ", *(*(a+i)+j)); 
        }
        printf("\n");
    }

    printf("\nArray b = \n"); 
    for(i=0;i<MAX_ROWS;i++)
    {
        for(j=0; j<MAX_COLUMNS; j++)
        {
            printf("%d  ", *(*(b+i)+j)); 
        }
        printf("\n");
    }

    //calculate the sum 
    for(i=0;i<MAX_ROWS;i++)
    {
        for(j=0; j<MAX_COLUMNS; j++)
        {
            *(*(c+i)+j) = (*(*(a+i)+j)) + (*(*(b+i)+j)); 
        }
    }

    printf("\nArray c = \n"); 
    for(i=0;i<MAX_ROWS;i++)
    {
        for(j=0; j<MAX_COLUMNS; j++)
        {
            printf("%d  ", *(*(c+i)+j)); 
        }
        printf("\n");
    }

    return 0x0; 
}

//Program to multiply two 2D arrays 

static int a[2][2] = {1, 2, 3, 4}; 
static int b[2][2] = {5, 6, 7, 8}; 
static int c[2][2] = {0}; 

#include <stdio.h>

int main() 
{
    int i= 0; 
    int j= 0;
    int k= 0;
    int sum = 0; 
    printf("\nPrinting a array = \n"); 
    //print array 1 
    for(i=0; i<2; i++)
    {
        for(j=0; j<2; j++)
        {
            printf("%d  ", a[i][j]);
        }
        printf("\n");
    }
    printf("\nPrinting b array = \n"); 
    //print array 2 
    for(i=0; i<2; i++)
    {
        for(j=0; j<2; j++)
        {
            printf("%d  ", b[i][j]);
        }
        printf("\n");
    }
    
    for (i = 0; i<2; i++)
    {
        for(j=0; j<2 ; j++)
        {
            c[i][j] = 0; 
            for(k=0; k<2; k++)
            {
                c[i][j] = c[i][j] + a[i][k]*b[k][j]; 
            }
        }
    }
    printf("\nProduct of two arrays axb = \n");
    
    for(i=0; i<2; i++)
    {
        for(j=0; j<2; j++)
        {
            printf("%d  ", c[i][j]);
        }
        printf("\n");
    }

    return 0;
}

//programs to find the sum of rows in a 2-D array

#include <stdio.h>

//initialize a 2 D array 

static int array[3][3] = {1,2,3,6,9,12,15,21,27}; 

int main(void)
{
    int i =0; 
    int j =0; 
    //array to hold the row sum
    int sum[3]= {0}; 

    //compute the sum of the rows 
    for(i=0; i<3; i++)
    {
        for(j=0; j<3; j++)
        {
            sum[i] = sum[i]+array[i][j]; 
        }
    }

    //display the result of the same 
    printf("\nThe array and the results are as follows -->\n"); 
    for(i=0; i<3; i++)
    {
        printf("\n");
        for(j=0; j<3; j++)
        {
            printf("%d  ", array[i][j]);
        }

        printf("SUM = %d",sum[i]);
    }

    return 0; 

}

//program to find the transpose of a matrix

#include<stdio.h>

#define MAX_ROWS        10 
#define MAX_COLOUMNS    10

static int array[MAX_ROWS][MAX_COLOUMNS]; 

static int transpose[MAX_ROWS][MAX_COLOUMNS]; 

int main(void)
{
    int m = 0; //rows 
    int n = 0; //columns
    int i =0; 
    int j =0; 

    printf("\nEnter mxn = \n");
    scanf("%d %d", &m, &n); 

    printf("\nEnter the array \n"); 
    for(i =0; i<m; i++)
    {
        for(j=0; j<n; j++)
        {
            scanf("%d", &array[i][j]); 
        }
    }

    printf("\n The original array is = \n");
    for(i =0; i<m; i++)
    {
        for(j=0; j<n; j++)
        {
            printf("%d   ", array[i][j]); 
        }
        printf("\n");
    }

    for(i =0; i<n; i++)
    {
        for(j=0; j<m; j++)
        {
            transpose[i][j]= array[j][i];
        }
    }


    printf("\n The transpose of array os = \n"); 
    for(i =0; i<n ; i++)
    {
        for(j=0; j<m; j++)
        {
            printf("%d   ", transpose[i][j]); 
        }
        printf("\n");
    }
    printf("\n");
    return 0x0;
}

//program to print the diagonal of a matrix

#include<stdio.h>

#define MAX_ROWS        10 
#define MAX_COLOUMNS    10

static int array[MAX_ROWS][MAX_COLOUMNS]; 


int main(void)
{
    int m = 0; //rows 
    int n = 0; //columns
    int i =0; 
    int j =0; 

    printf("\nEnter mxn = \n");
    scanf("%d %d", &m, &n); 

    if(m != n)
    {
        printf("\n Its not a square matrix hence program ends!!\n"); 
        goto END; 
    }

    printf("\nEnter the array \n"); 
    for(i =0; i<m; i++)
    {
        for(j=0; j<n; j++)
        {
            scanf("%d", &array[i][j]); 
        }
    }

    printf("\n The original array is = \n");
    for(i =0; i<m; i++)
    {
        for(j=0; j<n; j++)
        {
            printf("%d   ", array[i][j]); 
        }
        printf("\n");
    }

    printf("The Diagonal of the array is = ");
    for(i=0; i<m; i++)
    {
        printf("%d \n", array[i][i]); 
        printf("\t");
    }

    
    printf("\n");

END: 
    return 0x0;
}


//Program to print the Lower triangle of a 3x3 matrix
#include<stdio.h>

#define ROW         3
#define COLOUMN     3

int array[ROW][COLOUMN] = {0}; 

int main(void)
{
    int i =0; 
    int j =0; 

    printf("\nEnter the array\n"); 
    for(i=0; i<ROW; i++)
    {
        for(j=0; j<COLOUMN;j++)
        {
            scanf("%d", &array[i][j]); 
        }
    }

    printf("\n Given Matrix is = \n");

    for(i =0; i<ROW; i++)
    {
        for(j=0; j<COLOUMN; j++)
        {
            printf("%d   ", array[i][j]); 
        }
        printf("\n");
    }

    printf("\n The lower triangle of matrix is = \n");
    for(i =0; i<ROW; i++)
    {
        for(j=0; j<COLOUMN; j++)
        {
            if(i>=j)
            {
                printf("%d    ", array[i][j]);
            }
            else
            {
                printf("%d    ", 0x0); 
            }
        }
        printf("\n");
    }
    printf("\n");
    return 0x0;
}

//Write a program to change the endian-ness of a system 
//if i = 0xddccbbaa; 
//littleendian = 0xaa, bb , cc ,dd
//big endian = dd, cc, bb, aa

#include <stdio.h>

int main(void)
{
    int number = 0xddccbbaa; 

    printf("Original Number = 0x%x\n", number);

    number = (((number >>24) & 0x000000ff)
             |((number>>8) & 0x0000ff00)
             |((number<<24) & 0xff000000)
             |((number<<8) & 0x00ff0000)); 

    printf("\n Changed Endianness Number = 0x%x\n", number);

    return 0;
}

/* Given an integer an N. The task is to print the position of first set bit found from right side 
in the binary representation of the number.
*/
#include <stdio.h> 

typedef int int32_t; 

int main(void) 
{
    int32_t number = 0xdeadbee0; 
    int sizeOfNumber = (int)(sizeof(int32_t)*8);
    int i =0; 
    int count = 0x0; 

    if(number == 0)
    {
        printf ("No 0b1 in the number given \n");
    }

    for (i =0; i< sizeOfNumber; i++)
    {
        if((number&0x1) == 0x0)
        {
            count++; 
            number = number >> 0x1; 
            continue; 
        }
        else /* if it is a 0x1 */ 
        {
            break; 
        }
    }

    /* You need to add 1 for stupid people*/
    printf("The position of first set bit is = %d \n", (count+1));

    return 0; 
}

/*Given two numbers M and N. 
The task is to find the position of rightmost different bit in binary representation of numbers
*/

/*
Explanation:
Tescase 1: Binary representaion of the given numbers are: 1011 and 1001, 2nd bit from right is different.
*/

#include <stdio.h> 

typedef int int32_t; 

int main(void) 
{
    int numOne = 0x9; 
    int numTwo = 0xb; 
    int m1 = 0; 
    int n2 = 0; 
    int i = 0; 
    int count = 0; 

    for (i=0; i<32; i++)
    {
        m1 = numOne & 0x1; 
        n2 = numTwo & 0x1; 
        if(m1 == n2)
        {
            count++; 
            numOne >>= 0x1; 
            numTwo >>= 0x1; 
            continue; 
        }
        else
        {
            break; 
        }
    }

    printf("The first different bit position is %d \n",(count+1));
    return 0;
}

/*Program to know where what variables are*/
/* stored*/ 
#include <stdio.h>

int var1 = 1000; 

static int j; 

int main(void)
{
    int sum =0; 
    printf("\n address of var1 = 0x%x", &var1);
    printf("\n address of j = 0x%x", &j);
    printf("\n address of sum = 0x%x", &sum);
    return 0;
}

/*Program to know where what variables are*/
/* stored*/ 
#include <stdio.h>
#include <stdlib.h>
int var1 = 1000; 

static int j; 

int main(void)
{
    int sum =0; 
    char* mallocPtr = NULL; 
    char* callocPtr = NULL; 

    mallocPtr = (char*)malloc(2); 
    if(NULL != mallocPtr)
    {
        printf("\nmallocPtr location = 0x%x",mallocPtr);
        printf("\n data in mallocPtr = 0x%x, 0x%x", *(mallocPtr), *(mallocPtr+1));
    }
    callocPtr = (char*)calloc(2,1); 
    if(NULL != callocPtr)
    {
        printf("\n callocPtr location = 0x%x",callocPtr);
        printf("\n data in callocPtr = 0x%x, 0x%x", *(callocPtr), *(callocPtr+1));
    }
    printf("\n address of var1 = 0x%x", &var1);
    printf("\n address of j = 0x%x", &j);
    printf("\n address of sum = 0x%x", &sum);

    printf("\n size of integer here = %lu",sizeof(var1));
    free(mallocPtr); 
    free(callocPtr); 
    return 0;
}

// freeing memory in c++ 
#include <iostream>
using namespace std; 
#define SIZE_OF_BYTES 10

int main(void)
{
    int* ptr = NULL; 
    ptr = new int[SIZE_OF_BYTES]; 
    if(NULL != ptr)
    {
        cout<<"Alles Goed met het pointer" <<endl; 
    }
    else
    {
        cout<<"Ptr is kapot"<<endl;
    }
    for(int i=0; i<SIZE_OF_BYTES; i++)
    {
        ptr[i] = i;  
    }
    cout<<"Pointer contents are : " << endl;
    for(int j=0; j<SIZE_OF_BYTES; j++)
    {
        cout<<"0x"<<std::hex<< ptr[j] << endl; 
    }
    delete [] ptr; 
    return 0; 
}

// printing in binary in c++ 
#include <iostream>
#include <bitset>
using namespace std; 

int main(void)
{
    int var = 0xdeadbeef; 
    cout << " var = " << std::bitset<32>(var) << endl;
    return 0;
}


//create a print binary function
void binaryPrint(T_UINT16 variable)
{
    int i = 0; 
    int temp = 0; 
    int mask = 0x8000; 
    printf("0b ");
    for(i=0; i<16; i++)
    {
        temp = variable & mask; 
        temp = temp >>15; 
        variable = variable << 0x1; 
        printf("%d",temp); 
    }
}

/*Write a C program to swap the bytes of a 16-bit number*/
/* example 0xDEAD should be 0xADDE*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

int main(void)
{
    UINT16 variable = 0xDEAD; 
    UINT16 temp = 0x0; 
    printf("Original Number is   0x%x",variable);
    temp = variable & 0x00FF; 
    temp = temp << 8;  
    printf("\n temp   0x%x",temp);
    variable = variable >> 8; 
    variable = variable | temp; 
    printf("\nByte Wapped Number is   0x%x",variable);
    return 0x0; 
}


/*Write a program to count the number of trailing zeros*/
/*in an integer*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

typedef unsigned char UINT8; 
typedef unsigned short int UINT16;
typedef unsigned int UINT32;  

//create a print binary function
void binaryPrint(UINT16 variable)
{
    int i = 0; 
    int temp = 0; 
    int mask = 0x8000; 
    printf("0b ");
    for(i=0; i<16; i++)
    {
        temp = variable & mask; 
        temp = temp >>15; 
        variable = variable << 0x1; 
        printf("%d",temp); 
    }
}

//create a print binary function
void binary32Print(UINT32 variable)
{
    UINT32 i = 0; 
    UINT32 temp = 0; 
    UINT32 mask = 0x80000000; 
    printf("0b ");
    for(i=0; i<32; i++)
    {
        temp = variable & mask; 
        temp = temp >> 31; 
        variable = variable << 0x1; 
        printf("%d",temp); 
    }
}
/*Write a program to count the number of trailing zeros*/
/*in an integer*/
int main(void)
{
    UINT32 number = 0xffff0000; 
    UINT32 temp = 0; 
    UINT8 counter = 0x0; 
    UINT8 i = 0;  
    printf("\nThe original number is =");
    binary32Print(number);
    for(i=0; i<32; i++)
    {
        temp = number & 0x1; 
        if(temp == 0x0)
        {
            counter++;
            number = number >> 0x1;  
        }
        else
        {
            break;
        }
    }
    printf("\nThe number of trailing zeros = %d",counter);
    return 0x0; 
}

/*Print the Highest bit set*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

typedef unsigned char UINT8; 
typedef unsigned short int UINT16;
typedef unsigned int UINT32;  

//create a print binary function
void binaryPrint(UINT16 variable)
{
    int i = 0; 
    int temp = 0; 
    int mask = 0x8000; 
    printf("0b ");
    for(i=0; i<16; i++)
    {
        temp = variable & mask; 
        temp = temp >>15; 
        variable = variable << 0x1; 
        printf("%d",temp); 
    }
}

//create a print binary function
void binary32Print(UINT32 variable)
{
    UINT32 i = 0; 
    UINT32 temp = 0; 
    UINT32 mask = 0x80000000; 
    printf("0b ");
    for(i=0; i<32; i++)
    {
        temp = variable & mask; 
        temp = temp >> 31; 
        variable = variable << 0x1; 
        printf("%d",temp); 
    }
}

int main(void)
{
    UINT32 number =0x0000F000; 
    UINT32 duplicate = number; 
    UINT8 highestBitSet = 0x0; 
    int i = 0x0; 
    UINT32 temp = 0x0; 
    for(i=31; i>=0; i--)
    {
        temp = number & 0x80000000; 
        temp = temp >> 31; 
        if(temp == 0x1)
        {
            highestBitSet = i; 
            break;
        }
        else
        {
            number = number << 0x1; 
        }
    }
    printf("\n The original Number in binary is = ");
    binary32Print(duplicate); 
    printf("\n The highest bit set is %d",highestBitSet); 
    return 0x0; 
}

/* Print the negative number */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

typedef unsigned char UINT8; 
typedef unsigned short int UINT16;
typedef unsigned int UINT32;  

//create a print binary function
void binary32Print(UINT32 variable)
{
    UINT32 i = 0; 
    UINT32 temp = 0; 
    UINT32 mask = 0x80000000; 
    printf("0b ");
    for(i=0; i<32; i++)
    {
        temp = variable & mask; 
        temp = temp >> 31; 
        variable = variable << 0x1; 
        printf("%d",temp); 
    }
}

int main(void)
{
    int number = -2; 
    binary32Print(number);
    return 0x0; 
}

/*Bitwise way of checking if two numbers have opposite signs*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

typedef unsigned char UINT8; 
typedef unsigned short int UINT16;
typedef unsigned int UINT32;  

int main(void)
{
    int num1 = -1; 
    int num2 = -12; 
    int res = 0x0; 
    res = num1^num2; 
    if(res < 0)
    {
        printf("The numbers have different signs"); 
    }
    else
    {
        printf("The numbers have same sign");
    }
    return 0; 
}

/*Write a program to rotate n number of bits by m spaces*/
/* and the bits shouldn't be lost */ 
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

typedef unsigned char UINT8; 
typedef unsigned short int UINT16;
typedef unsigned int UINT32;  


UINT32 rotateBinary(UINT32 number, UINT8 noOfPositions, bool isRightShift)
{
    UINT32 temp1 = 0x0; 
    UINT32 temp2= 0x0; 
    UINT32 final_result = 0x0;
    if(false == isRightShift)
    {
        temp1 = number << noOfPositions; 
        temp2 = number >> (32-noOfPositions);
    }
    else
    {
        temp1 = number >> noOfPositions; 
        temp2 = number << (32-noOfPositions); 
    }
    final_result = temp1 | temp2; 
    return final_result; 
} 

int main(void)
{
    int number = 32;
    int result = 0x0; 
    result = rotateBinary(number,2,true);
    printf("\nRight Shifted number = 0x%x",result); 
    result= result ^ result; 
    result = rotateBinary(number,2, false); 
    printf("\nLeft Shifted number = 0x%x",result); 
    return 0;  
}

/*Program to multiply two numbers with bitwise operators*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

typedef unsigned char UINT8; 
typedef unsigned short int UINT16;
typedef unsigned int UINT32;  

int main(void)
{
    UINT32 number = 1000; 
    printf("\nOriginal number = %u",number); 
    number = number << 1; 
    printf("\n Number multiplied by 2 is =%u", number); 
    return 0; 
}

/*Using function pointer add two numbers*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

typedef unsigned char UINT8; 
typedef unsigned short int UINT16;
typedef unsigned int UINT32;  

int SumOfTwoNumbers(int a, int b)
{
    int sum = 0x0; 
    sum = a+b; 
    return(sum); 
}

int (*sumPtr)(int,int); 

int main(void)
{
    int num1 = 4500;
    int num2 = 2415; 
    sumPtr = &SumOfTwoNumbers; 
    printf("Sum of Numbers = %u",(*sumPtr)(num1,num2));
    return 0; 
}

/*Use function pointers for passing as a parameter to function*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

typedef unsigned char UINT8; 
typedef unsigned short int UINT16;
typedef unsigned int UINT32;  


bool isReadyToExit = false; 

void CallbackFunction(void)
{
    isReadyToExit = true; 
}

void Print1000Numbers(void(*t_callback)(void))
{
    int i = 0; 
    if(NULL == t_callback)
    {
        printf("\nERROR: Null pointer"); 
    }
    else
    {
        for(i=0; i<1001; i++)
        {
            printf("\n %d",i);
        }
        (*t_callback)(); //call the callback 
    }
}

int main(void)
{
    Print1000Numbers(&CallbackFunction); 
    while(!isReadyToExit)
    {
        ; 
    }
    printf("\n Ready to exit"); 
    return 0; 
}
