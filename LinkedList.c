//Program to play around with Linked List 

#include<stdio.h>
#include<stdlib.h>

struct array
{
	int val;
	struct array* next;  

}; 

static struct array* CreateANode(int val)
{
	struct array* node = NULL; 
	node = (struct array*)malloc(sizeof(struct array));
	if(NULL!=node)
	{
		node->val= val; 
		node->next =NULL; 
	}
	else
	{
		printf("\nERROR!! No memory ");
	}
	return node;
}

static struct array* CreateALinkedList(int noOfElements)
{
	struct array* ptr = NULL; 
	struct array* head = NULL;
	int i;  
	head = CreateANode(0x0); 
	ptr = head; 
	for(i =1; i< (noOfElements); i++)
	{
		ptr->next = CreateANode(i);
		ptr = ptr->next; 
	}
	return head;
}

static void PrintTheLinkedList(struct array* head)
{
	struct array* ptr = NULL; 
	if(NULL!=head)
	{
		ptr = head; 
		while(ptr!= NULL)
		{
			printf("%d    ", ptr->val); 
			ptr = ptr->next;
		}
	}
}

static void ReverseALinkedList(struct array* head)
{
	struct array* Curr = NULL; 
	struct array* Prev = NULL; 
	struct array* Next = NULL; 

	//Current = Head; 
	Curr = head; 

	while(Curr != NULL)
	{
		Next = Curr->next; 
		Curr->next = Prev; 
		Prev = Curr; 
		Curr = Next; 
	}
	head = Prev; 
	PrintTheLinkedList(head); 
}

int main(void)
{
	struct array* data = NULL; 
	struct array* reversed = NULL; 

	data = CreateALinkedList(5); 
	PrintTheLinkedList(data); 
	
	printf("\nReversed a Linked List\n"); 
	//Reverse  a linked List
	ReverseALinkedList(data); 

	printf("\n");
	return 0;
}